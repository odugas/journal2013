\section{Description du filtre de Kalman basé sur la transformation non parfumée}
\label{Chap3Sec1UKF}

\subsection{Description de l'algorithme}
\label{C3S1SS1}

À la section~\ref{C1S1Filter}, nous citons le livre de Thrun, Burgard et Fox~\cite{thrun2005probabilistic}, qui présente en détail diverses techniques de filtrage probabilistes. La majorité d'entre eux ont en commun le fait qu'ils implémentent deux phases. La première phase, appelée la prédiction, combine la pose précédente estimée d'un robot avec les plus récentes commandes envoyées à ce dernier afin de prédire sa pose actuelle. De plus, à partir de la prédiction de la pose actuelle, les filtres probabilistes tentent de prédire les mesures que retournent les capteurs du robot à cette pose prédite. La seconde étape est nommée la mise à jour. Lors de celle-ci, les mesures actuelles sont intégrées aux prédictions afin d'obtenir une estimation précise de la pose actuelle du robot.

Chaque étape des filtres probabilistes utilise des données qui sont, dans la plupart des cas réels, des fonctions non linéaires. En effet, les commandes envoyées aux moteurs des robots peuvent résulter en une fonction de déplacement non linéaire. Par exemple, un robot à qui nous appliquons une vitesse de translation et de rotation constante se déplace typiquement sur une trajectoire circulaire, ce qui ne peut être décrit par une transition d'état linéaire. De la même manière, la mesure d'un capteur de distance, comme un télémètre laser, nous fournit des estimations de distances selon une fonction non linéaire. Pour cette raison, plusieurs filtres emploient des techniques pour linéariser les mesures et les transitions d'état. Certains, comme le filtre étendu de Kalman, appliquent des expansions en séries de Taylor. D'autres filtres, comme le filtre de Kalman basé sur la transformation non parfumée (\emph{UKF}), utilisent une méthode de linéarisation déterministe grâce à l'utilisation d'un processus de régression linéaire statistique pesée. En d'autres termes, à partir de la gaussienne qui estime la pose d'un agent, le filtre UKF extrait une série de points, dits points sigma. L'un de ces points est situé à la pose moyenne estimée. Les autres se trouvent le long des principaux axes de covariance de la gaussienne de cette pose et sont au nombre de deux par dimension de la pose. 

La figure~\ref{figC3S1SigmaPoints} illustre l'extraction des points sigma selon le filtre UKF ainsi que l'utilité de ceux-ci. Dans cette image représentant une estimation de pose en deux dimensions, cinq points sigma sont extraits de la gaussienne exprimant l'estimation. Après avoir subi une transition d'état non linéaire, les points sigma sont utilisés afin de déduire la distribution non linéaire de cette transition. Suite à une linéarisation utilisant le poids des points sigma, la pose actuelle peut être estimée avec grande précision.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.65\textwidth]{../Figures/C3S1SigmaPoints.png}
  \end{tabular}
 \end{center}
 \caption[Les points sigma du filtre UKF]{Les points sigma du filtre UKF. Version modifiée de Van Der Merwe~\cite{van2004sigma}}
 \label{figC3S1SigmaPoints}
\end{figure}

Plusieurs variantes du filtre UKF sont présentées dans le livre de Thrun, Burgard et Fox~\cite{thrun2005probabilistic}. Le filtre général pose l'hypothèse que le bruit sur la prédiction et celui sur les mesures sont additifs. Cette hypothèse rend le filtre plus simple à implémenter parce qu'il permet l'addition des covariances de l'état et des mesures aux incertitudes prédites. Afin d'estimer la position relative de nos agents mutuellement observables, nous utilisons plutôt une variante de ce filtre. Cette variante implique l'augmentation de la taille des vecteurs et matrices d'estimation de l'état en y incorporant d'autres composantes, soit les commandes de déplacement et les mesures des capteurs. Ainsi, le vecteur d'état augmenté, dorénavant noté $\vec{\mu^a_0}$, possède une dimensionnalité $n$. 

L'algorithme de la variante du filtre UKF que nous utilisons peut être décrit comme suit. Tout d'abord, il faut établir les paramètres en entrée du filtre. Définissons par le vecteur $\vec{\mu_0}$ la position relative moyenne estimée du $Robot_B$ au temps $t_0$. La covariance de $\vec{\mu_0}$ correspond à la matrice $\mathbf{\Sigma_0}$. Au temps $t_1$, nous avons aussi la commande de transition d'état $\vec{u_1}$ avec l'incertitude $\mathbf{\Sigma_X}$ qui doit être ajoutée à la covariance $\mathbf{\Sigma_0}$ suite à cette transition. Enfin, nous considérons les mesures des capteurs $\vec{z_1}$ ainsi que l'incertitude $\mathbf{\Sigma_X}$ sur ces mesures. La dimensionnalité $n$ de $\vec{\mu^a_0}$ correspond à la somme des dimensionnalités de $\vec{\mu_0}$, de $\vec{u_1}$ et de $\vec{z_1}$.

À l'initialisation du filtre, quelques constantes sont définies. D'abord, considérons les paramètres de mise à l'échelle $\alpha$, $\beta$ et $\kappa$. Ceux-ci permettent d'incorporer des connaissances additionnelles sur la distribution sous-jacente à la représentation gaussienne de l'état. En effet, $\alpha$ et $\kappa$ déterminent l'éloignement des points sigma par rapport à la moyenne, tandis que $\beta$ permet d'exprimer une distorsion connue dans la représentation gaussienne de l'état. Puisque nous posons l'hypothèse que cette distribution nous est inconnue, nous préservons la valeur par défaut de ces paramètres, qui est mentionnée dans~\cite{thrun2005probabilistic}. À partir de ces paramètres, nous définissons $\lambda$ et $\gamma$, deux paramètres utilisés dans le calcul des points sigma, par les équations \ref{c3s1eqlambda} et~\ref{c3s1eqgamma}.
\begin{equation}
\label{c3s1eqlambda}
\lambda = (\alpha^2(n+\kappa)-n)
\end{equation}
\begin{equation}
\label{c3s1eqgamma}
\gamma = \sqrt{n+\lambda}
\end{equation}

Deux poids sont associés à chaque point sigma, soit $\vec{w_m}$ et $\vec{w_c}$. $\vec{w_m}$ est utilisé lors de la prédiction de l'état augmenté moyen au temps $t_1$. Le poids $w^0_m$ du point sigma principal (c.-à-d. celui positionné à la moyenne $\vec{\mu^a_0}$) est déterminé comme suit~:
\begin{equation}
w^0_m = \frac{\lambda}{n+\lambda}
\end{equation}

Les autres points sigma obtiennent le poids $w^i_m$, où $i=1,...,2n$~:
\begin{equation}
w^i_m = \frac{1}{2(n+\lambda)}
\end{equation}

Le vecteur de poids $\vec{w_c}$ est employé pour retrouver la covariance de cette prédiction de l'état augmenté moyen au temps $t_1$. Ces poids sont définis par
\begin{equation}
w^0_c = \frac{\lambda}{n+\lambda} + (1 - \alpha^2 + \beta)
\end{equation}
et, pour $i=1,...,2n$, 
\begin{equation}
w^i_c = w^i_m.
\end{equation}

Considérons maintenant l'algorithme du filtre UKF en procédant étape par étape. Tout d'abord, il faut construire l'état augmenté $\vec{\mu^a_0}$. Puisque nous posons l'hypothèse d'un bruit gaussien de moyenne centrée à zéro pour tout bruit dans notre système, $\vec{\mu^a_0}$ est donné par la moyenne de l'état estimé $\vec{\mu_0}$ combiné à des vecteurs de zéro pour le bruit estimé sur la commande et sur les mesures, tel que décrit par l'équation 
\begin{equation}
\label{c3s1mua0}
\vec{\mu^a_0} = \left(\vec{\mu^T_0} \,\, \left(\vec{0^T}\right) \,\, \left(\vec{0^T}\right)\right)^T
\end{equation}

De plus, la matrice de covariance $\mathbf{\Sigma^a_0}$ est augmentée en ajoutant dans la diagonale de celle-ci les covariances $\mathbf{\Sigma_0}$, $\mathbf{\Sigma_X}$ et $\mathbf{\Sigma_X}$.
\begin{equation}
\label{c3s1sigmaa0}
\mathbf{\Sigma^a_0} = \begin{bmatrix}
\mathbf{\Sigma_0} & \mathbf{0} & \mathbf{0} \\
\mathbf{0} & \mathbf{\Sigma_X} & \mathbf{0} \\
\mathbf{0} & \mathbf{0} & \mathbf{\Sigma_X}
\end{bmatrix}
\end{equation}

Une fois l'état augmenté, les points sigma sont générés par l'équation\ref{c3s1makesigma}. La notation $\mathbf{\chi^a_0}$ implique que cette équation résulte en une matrice de vecteurs verticaux de dimension $n$ représentant l'état au temps $t_0$. Il est important de noter que pour calculer la racine carrée d'une matrice dans un filtre probabiliste, Thrun, Burgard et Fox~\cite{thrun2005probabilistic} indiquent qu'il faut utiliser la décomposition de Cholesky, technique considérée stable et numériquement efficace.
\begin{equation}
\label{c3s1makesigma}
\mathbf{\chi^a_0} = \begin{bmatrix}
\vec{\mu^a_0} & \left(\vec{\mu^a_0} + \gamma\sqrt{\mathbf{\Sigma^a_0}}\right) & \left(\vec{\mu^a_0} - \gamma\sqrt{\mathbf{\Sigma^a_0}}\right)
\end{bmatrix}
\end{equation}

L'étape suivante est la transition à l'état prédit des points sigma $\mathbf{\chi^a_1}$. Cela est complété à l'aide d'une fonction arbitraire non linéaire $g$ appliquée aux points sigma $\mathbf{\chi^a_0}$
\begin{equation}
\label{c3s1sigmapredict}
\mathbf{\chi^x_1} = g\left( \vec{u_1} + \mathbf{\chi^u_0}, \mathbf{\chi^x_0}\right)
\end{equation} 
où $\mathbf{\chi^x}$ représente la partie de $\mathbf{\chi^a}$ liée à la localisation $\vec{\mu_0}$ et où $\mathbf{\chi^u}$ représente la partie de $\mathbf{\chi^a}$ liée au bruit sur la transition d'état $\vec{u_1}$. 

À partir de l'équation~\ref{c3s1sigmapredict}, il est possible d'évaluer la prédiction $\vec{\mu^p_1}$ du nouvel état. Les points $\mathbf{\chi^x_1}$ ainsi calculés sont utilisés avec les poids $\vec{w_m}$ et $\vec{w_c}$ des points sigma afin d'obtenir notre prédiction $\vec{\mu^p_1}$ de l'état à $t_1$ ainsi que sa covariance $\mathbf{\Sigma^p_1}$.
\begin{equation}
\label{c3s1mupredict}
\vec{\mu^p_1} = \sum\limits_{i=0}^{2n} w^i_m \, \mathbf{\chi^{x,i}_1}
\end{equation} 
\begin{equation}
\label{c3s1covariancepredict}
\mathbf{\Sigma^p_1} = \sum\limits_{i=0}^{2n} w^i_c \, \left( \mathbf{\chi^{x,i}_1} - \vec{\mu^p_1} \right) \, \left( \mathbf{\chi^{x,i}_1} - \vec{\mu^p_1} \right)^T
\end{equation}

Par la suite, il est possible de prédire les observations $\mathbf{Z^p_1}$ mesurées aux points sigma $\mathbf{\chi^a_1}$ au temps $t_1$. Cela est accompli grâce à une fonction arbitraire non linéaire $h$ telle qu'illustrée à l'équation~\ref{c3s1zpredict} 
\begin{equation}
\label{c3s1zpredict}
\mathbf{Z^p_1} = h\left(  \mathbf{\chi^x_1} \right) + \mathbf{\chi^z_0}
\end{equation}
où $\mathbf{\chi^z_0}$ représente la partie de $\mathbf{\chi^a_0}$ liée au bruit sur les mesures $\vec{z_1}$. Nous pouvons ensuite combiner les mesures $\mathbf{Z^p_1}$ aux points sigma $\mathbf{\chi^x_1}$ grâce aux poids de ces derniers comme suit~:
\begin{equation}
\label{c3s1zp}
\vec{z^p_1} = \sum\limits_{i=0}^{2n} w^i_m \, \mathbf{Z^{p,i}_1}.
\end{equation}

Puis, la matrice de covariance $\mathbf{S_1}$ sur les prédictions des mesures aux équations \ref{c3s1zpredict} et \ref{c3s1zp} est évaluée tel qu'indiqué à l'équation~\ref{c3s1S}.
\begin{equation}
\label{c3s1S}
\mathbf{S_1} = \sum\limits_{i=0}^{2n} w^i_c \, \left( \mathbf{Z^{p,i}_1} - \vec{z^p_1} \right) \, \left( \mathbf{Z^{p,i}_1} - \vec{z^p_1} \right)^T
\end{equation}

Dans le but d'utiliser les mesures $\vec{z_1}$ pour estimer l'état $\mu_1$ à partir de sa prédiction $\mu^p_1$, tous les filtres gaussiens dérivant du filtre de Kalman impliquent de calculer une matrice $\mathbf{K_1}$ appelée le gain de Kalman. Pour trouver $\mathbf{K_1}$, il faut utiliser les covariances $\mathbf{\Sigma^p_1}$ et $\mathbf{S_1}$ pour calculer la matrice de covariances croisées, notée $\mathbf{\Sigma^{x,z}_1}$.
\begin{equation}
\mathbf{\Sigma^{x,z}_1} = \sum\limits_{i=0}^{2n} w^i_c \, \left( \mathbf{\chi^{x,i}_1} - \vec{\mu^p_1} \right) \, \left( \mathbf{Z^{p,i}_1} - \vec{z^p_1} \right)^T
\end{equation}

S'en suit directement l'évaluation du gain de Kalman $\mathbf{K_1}$ à l'équation~\ref{c3s1kalmangain}. Cette matrice est utilisée à l'équation~\ref{c3s1mu} pour appliquer une modification à $\vec{\mu^p_1}$ en incorporant la connaissance additionnelle que nous procure la comparaison entre la mesure $\vec{z_1}$ et sa prédiction $\vec{z^p_1}$. Également, à l'équation~\ref{c3s1sigma}, $\mathbf{K_1}$ permet de déterminer la matrice de covariance $\mathbf{\Sigma_1}$ qui nous donne l'erreur sur l'estimation de $\vec{\mu_1}$.
\begin{equation}
\label{c3s1kalmangain}
\mathbf{K_1} = \mathbf{\Sigma^{x,z}_1} \, \mathbf{S_1}^{-1}
\end{equation}
\begin{equation}
\label{c3s1mu}
\vec{\mu_1} = \vec{\mu^p_1} + \mathbf{K_1} \left( \vec{z_1} - \vec{z^p_1} \right)
\end{equation}
\begin{equation}
\label{c3s1sigma}
\mathbf{\Sigma_1} = \mathbf{\Sigma^p_1} - \mathbf{K_1} \mathbf{S_1} \mathbf{K_1}^T
\end{equation}



\subsection{Simulation et résultats}
\label{C3S1SS2}

À la section~\ref{Chap1Seq2QHYPG}, nous présentons une description des tryphons~\cite{sharf2012}.  Le système de localisation qui est détaillé au \ref{Chapitre2} a été conçu en gardant en tête les spécifications de ces cubes flottants. Pour analyser l'efficacité du filtre UKF à faire le suivi de la position de ce robot, nous avons réalisé une simulation modélisant les mouvements dans l'espace de celui-ci. Dans cette simulation, nous avons défini les tryphons comme des objets pouvant se déplacer à une vitesse $v$ similaire à ce qui a été observé dans la réalité. Chaque commande $\vec{u_1}$ envoyée au robot est de la forme 
\begin{equation}
\vec{u_1} = v|\vec{(x,y,z)}|
\end{equation}
où $|\vec{(x,y,z)}|$ est un vecteur unitaire représentant une direction dans l'espace tridimensionnel. Afin de modéliser le bruit sur les déplacements, nous avons conçu une fonction d'erreur $\mathbf{\epsilon}$ simulant un effet de flottement du robot. $\mathbf{\epsilon}$ emploie l'intervalle de temps $\Delta_t$ de notre simulation, une fréquence sinusoïdale primaire de flottement $f_p$, un entier $i$ qui incrémente à chaque itération pour ajouter une saveur aléatoire, ainsi qu'un vecteur de norme $\vec{\phi}$ et un vecteur de fréquence secondaire $\vec{f_s}$ pour représenter des courants d'air différents sur les trois axes. L'équation~\ref{c3s1noisematrix} présente cette matrice d'erreur~$\mathbf{\epsilon}$.
\begin{equation}
\label{c3s1noisematrix}
\mathbf{\epsilon} = \begin{bmatrix}
U(0,1) \sin(2 \pi f \Delta_t i ) + \phi^x \sin(\vec{f^x_s}\Delta_t) \\
U(0,1) \sin(2 \pi f \Delta_t i ) + \phi^y \tan(\vec{f^y_s}\Delta_t) \\
U(0,1) \sin(2 \pi f \Delta_t i ) + \phi^z \cos(\vec{f^z_s}\Delta_t)
\end{bmatrix}
\end{equation}

Également, nous avons mentionné à la sous-section~\ref{C3S1SS2} que le filtre UKF utilise des fonctions arbitraires non linéaires $g$ et $h$ pour la prédiction de la transition d'état et pour la prédiction des mesures, respectivement. Dans notre simulation, nous remplaçons $g$ de l'équation générique~\ref{c3s1sigmapredict} par
\begin{equation}
\mathbf{\chi^x_1} = \mathbf{\chi^x_0} + \vec{u_1} + \mathbf{\chi^u_0},
\end{equation}
ce qui correspond à notre connaissance du problème réel. Pour ce qui est de la fonction $h$, nous utilisons la simulation présentée à la section~\ref{Chap2Sec2SIM} pour déterminer les angles $\alpha$ et $\beta$ de notre technique de localisation. Nous privilégions cette approche à l'utilisation comme mesure $\vec{z_1}$ de la translation $\vec{T}$ (voir l'équation~\ref{C2S1SS3T}) puisque cette dernière implique une perte d'information parce qu'il est difficile de connaître avec précision la covariance de ce type de mesures. Puisque nous connaissons l'erreur en pixel de nos caméras réelles, nous pouvons l'utiliser pour calculer la covariance $\Sigma_z$ lorsque $\vec{z_1}$ est le vecteur contenant les angles $\alpha$ et $\beta$. Ainsi, la fonction $h$ devient
\begin{equation}
\mathbf{Z^p_1} = \mbox{predictMeasurementAlphaBeta}\left(\mathbf{\chi^x_1}\right) +  \mathbf{\chi^z_0}
\end{equation}
où la fonction $\mbox{predictMeasurementAlphaBeta}$ retourne les angles $\alpha$ et $\beta$ lorsque la position de l'agent est envoyée en tant que paramètre d'entrée. Le lecteur est invité à consulter l'Appendice~\ref{AppendixD}, où il peut y trouver l'implémentation de cette variante du filtre UKF dans Matlab.

Pour cette simulation, notre agent se déplace dans l'espace tridimensionnel en tentant de suivre une trajectoire de la forme d'un carré. Cette manœuvre est répétée plusieurs fois. La figure~\ref{figC3S1UKFSim} présente les résultats de notre simulation.
\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=4.4cm 2cm 4.4cm 0cm, clip=true,  width=0.97\textwidth]{../Figures/C3S1UKFSim.png}
  \end{tabular}
 \end{center}
 \caption[Simulation du positionnement avec le filtre UKF]{Simulation du positionnement avec la variante choisie du filtre UKF. En haut à gauche, le graphique indiquant l'effet de notre fonction de bruit sur la commande $\vec{u_1}$ modélisant les courants d'air. En haut au centre, le bruit en centimètres résultant de la précision subpixel des caméras utilisées. En haut à droite, l'erreur sur $\vec{\mu_1}$ à la sortie du filtre UKF.}
 \label{figC3S1UKFSim}
\end{figure}

Comme nous pouvons le constater, malgré les mesures fortement bruitées, le filtre UKF adoucit l'erreur sur $\vec{\mu_1}$, c.-à-d. les estimations de la position de l'agent. Il s'agit du comportement souhaité ici, puisque nous voulons permettre aux tryphons de se localiser avec précision même au-delà de la limite de résolution des caméras utilisées. À chaque reprise de la simulation, les erreurs sur le positionnement des estimations de $\vec{mu_1}$ sont groupées et possèdent une variance beaucoup plus faible que les mesures $\vec{z_1}$.

Dans la prochaine section, nous allons présenter la technique de filtrage utilisée pour estimer l'orientation relative des agents se localisant mutuellement.


