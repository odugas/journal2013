\section{Simulation}
\label{Chap2Sec2SIM}

Cette section est consacrée à la simulation de la solution analytique. Cette simulation a pour objectif de représenter le plus fidèlement la réalité dans laquelle un tel système de localisation serait utilisé. En premier lieu, nous allons décrire la nature et les objectifs de la simulation, sa façon de représenter les robots et la technique utilisée pour simuler la génération d'image. En second lieu, nous allons expliquer comment est calculé chaque élément de la simulation, dans le but de permettre au lecteur de comprendre quelles méthodes sont utilisées pour valider les différents résultats qui sont produits par la simulation. En dernier lieu, nous allons présenter et expliquer ceux-ci en détail.

%-----------------------------------
%	DESCRIPTION
%-----------------------------------
\subsection{Définitions et représentation dans la simulation}
\label{C2S2SS1}

Notre simulation est décrite par les éléments suivants :
\begin{itemize}
\item Nous avons deux robots, $Robot_A$ et $Robot_B$. Le premier est immobile tandis que le second se déplace.
\item Sur chaque robot $i$ sont fixés une caméra $C_i$ et deux marqueurs visuels $L_i$ et $R_i$.
\item Les marqueurs $L_i$ et $R_i$ sont séparés par une distance $d$.
\item En tout temps, la caméra de chaque robot peut voir la caméra et les marqueurs de l'autre robot.
\item La caméra $C_A$ est modélisée avec sa propre longueur focale $f_A$ et il en va de même pour la caméra $C_B$ qui possède une longueur focale $f_B$.
\item Les caméras ont leur propre point central, qui définit l'intersection de l'axe optique avec le plan image de chaque caméra.
\item Les images sont saisies en même temps par les caméras $C_A$ et $C_B$.
\item Chaque image représente une projection du monde qui est mesurée en pixel et qui montre le point de vue de chaque caméra.
\item Bien que tous les paramètres soient connus par la simulation, notre méthode utilise uniquement la distance $d$, les focales $f_A$ et $f_B$ des caméras et les images synthétisées.
\end{itemize}

Pour la simulation, les matrices de rotations $4\times4$ en coordonnées homogènes sont représentées par :

\begin{equation}
\label{C2S2SS1eqROT}
\mathbf{R} = \mbox{RotationMatrix}(r_x, r_y, r_z)
\end{equation}

où $r_x$, $r_y$ et $r_z$ signifient respectivement un angle de rotation en radians autour de l'axe des $x$, $y$ et $z$. Ainsi, $^A_B\mathbf{R}$ se calcule par :

\begin{equation}
\begin{bmatrix}
c(r_y)c(r_z) & -c(r_x)s(r_z)+s(r_x)s(r_y)c(r_z) & s(r_x)s(r_z)+c(r_x)s(r_y)c(r_z) & 0 \\
c(r_y)s(r_z) & c(r_x)c(r_z)+s(r_x)s(r_y)s(r_z) & -s(r_x)c(r_z)+c(r_x)s(r_y)s(r_z) & 0 \\
-s(r_y) & s(r_x)c(r_y) & c(r_x)c(r_y) & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\end{equation}

où $c$ est la fonction cosinus et où $s$ est la fonction sinus.

Les matrices de translation $4\times4$ en coordonnées homogènes sont représentées par :

\begin{equation}
\label{C2S2SS1eqTRA}
\mathbf{T} = \mbox{TranslationMatrix}(t_x, t_y, t_z)
\end{equation}

où $t_x$, $t_y$ et $t_z$ signifient respectivement un déplacement en centimètres le long de l'axe des $x$, $y$ et $z$, tel que :
\begin{equation}
\mathbf{T} = \begin{bmatrix}
1 & 0 & 0 & t_x \\
0 & 1 & 0 & t_y \\
0 & 0 & 1 & t_z \\
0 & 0 & 0 & 1
\end{bmatrix}
\end{equation}

Chaque robot est défini par trois points réunis dans une matrice en coordonnées homogènes, $\mathbf{Robot_i}$, où $i$ signifie l'un des robots, soit $A$ ou $B$. Chaque colonne de la matrice d'un robot représente la position d'un de ces trois points. Le premier point représente le centre du robot, mais également la position où la caméra $C_i$ est située, car elle est centrée à $(0,0,0)$. Le second point correspond au marqueur de gauche, $\vec{L}_i$, et est positionné à $(d^L_i, 0,0) $. Pour ce qui est du troisième point, il s'agit du marqueur visuel droit, $\vec{R}_i$, et celui-ci est localisé à $(-d^R_i,0,0)$. Comme $d^L_i$ peut être différent de $d^R_i$, la caméra $\vec{C}_i$ n'est pas nécessairement directement à mi-distance des marqueurs lumineux, tel que décrit initialement. Ainsi, nos robots peuvent être exprimés par les matrices suivantes :
\begin{equation}
\mathbf{Robot_A} = \begin{bmatrix}
0 & d^L_A & -d^R_A \\
0 & 0 & 0 \\
0 & 0 & 0 \\
1 & 1 & 1
\end{bmatrix}
\end{equation}

\begin{equation}
\mathbf{Robot_B} = \begin{bmatrix}
0 & d^L_B & -d^R_B \\
0 & 0 & 0 \\
0 & 0 & 0 \\
1 & 1 & 1
\end{bmatrix}
\end{equation}

Le $\mathbf{Robot_B}$ est déplacé à l'aide d'une matrice de rotation et d'une matrice de translation arbitraire, avec pour seule contrainte que les robots doivent se faire face. Pour ce faire, une rotation est appliquée autour de l'axe des y de $\pi$ radians afin de faire en sorte de respecter cette contrainte :

\begin{equation}
\label{C2S2SS1eqR1}
^A_B\mathbf{R_{r1}} = \mbox{RotationMatrix}(0, \pi, 0)
\end{equation}

Nous avons ensuite appliqué une matrice de rotation additionnelle ainsi qu'une matrice de translation, les deux de manière tout à fait arbitraire :
\begin{equation}
\label{C2S2SS1eqR2}
^A_B\mathbf{R_{r2}} = \mbox{RotationMatrix}(\theta\,rad, \phi\,rad, \omega\,rad)
\end{equation}
\begin{equation}
\label{C2S2SS1eqT}
^A_B\mathbf{T} = \mbox{TranslationMatrix}(x\,cm, y\,cm, z\,cm)
\end{equation}

Ces transformations sont appliquées sur le $Robot_B$ de sorte que celui-ci soit positionné pour faire face au $\mathbf{Robot_A}$ :
\begin{equation}
\mathbf{Robot_B} = ^A_B\mathbf{T} \, ^A_B\mathbf{R_{r2}} \, ^A_B\mathbf{R_{r1}} \, \mathbf{Robot_B}
\end{equation}

Afin de simuler la prise d'image par les caméras $\vec{C}_A$ et $\vec{C}_B$, une longueur focale $f_i$ est définie pour chaque caméra $C_i$. Puis, la technique suivante est appliquée afin de créer l'image à deux dimensions $I_A$ représentant le point de vue du $Robot_A$. Étant donné que le $\mathbf{Robot_B}$ est défini dans le référentiel du $\mathbf{Robot_A}$, et parce que $\vec{C}_A$ est centré à $(0,0,0)$, une simple projection $\mathbf{P_A}$ telle que
\begin{equation}
\mathbf{P_A} = \begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1/f_A & 0
\end{bmatrix}
\end{equation}

permet de générer une image projetée $\mathbf{\mbox{Proj}_A}$ par simple multiplication de $\mathbf{P_A}$ avec la matrice $\mathbf{Robot_B}$.
\begin{equation}
\mathbf{\mbox{Proj}_A} = \mathbf{P_A}*\mathbf{Robot_B}
\end{equation}

Il est à remarquer que la caméra $\vec{C}_B$ ne fait pas partie de l'image, car seuls les marqueurs sont visibles. De plus, nous pouvons retrouver la position  de $\vec{C}_B$ à l'aide des calculs démontrés à la section~\ref{Chap2Sec1MATH}. Les équations suivantes permettent d'extraire de $\mathbf{\mbox{Proj}_A}$ les positions des marqueurs $L_B$ et $R_B$ dans l'image $\mathbf{I_A}$ :

\begin{equation}
\mathbf{I_A}(1,:) = \frac{\mathbf{\mbox{Proj}_A}(1,2:3)}{\mathbf{\mbox{Proj}_A}(3,2:3)}
\end{equation}
\begin{equation}
\mathbf{I_A}(2,:) = \frac{\mathbf{\mbox{Proj}_A}(2,2:3)}{\mathbf{\mbox{Proj}_A}(3,2:3)}
\end{equation}

Pour terminer, afin de représenter la réalité, un bruit gaussien en pixel de $\sigma_A$ est appliqué sur la position des marqueurs visuels dans l'image générée.
\begin{equation}
\mathbf{I_A} = \mathbf{I_A} + \mathcal{X}[2,2] \thicksim \mathcal{N}(0, \sigma^2_A)
\end{equation}
Ce bruit représente à la fois les imperfections de la caméra et les erreurs d'approximation de l'algorithme de détection des marqueurs lumineux. 

La génération de l'image $I_B$ n'est pas réalisée de la même manière, à la différence qu'il faut cette fois exprimer la position du $\mathbf{Robot_A}$ dans le repère $F_B$ du $\mathbf{Robot_B}$. Le $\mathbf{Robot_A}$ doit subir les transformations inverses de celles établies aux équations~\ref{C2S2SS1eqR1}, \ref{C2S2SS1eqR2} et~\ref{C2S2SS1eqT}.
\begin{equation}
\mathbf{\mathbf{Proj}_B} = \mathbf{P_B} *\, ^A_B\mathbf{R_{r1}}^{-1} *\, ^A_B\mathbf{R_{r2}}^{-1} *\, ^A_B\mathbf{T}^{-1} * \mathbf{Robot_A}
\end{equation}
Une nouvelle matrice de projection $\mathbf{P_B}$ est générée à cette fin.
\begin{equation}
\mathbf{P_B} = \begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1/f_B & 0
\end{bmatrix}
\end{equation}

Tout comme pour l'image $I_A$, la caméra $\vec{C}_A$ ne fait pas partie de l'image $I_B$, car seuls les marqueurs sont visibles. Les équations suivantes permettent d'extraire de $\mathbf{\mbox{Proj}_B}$ les positions des marqueurs $L_A$ et $R_A$ dans l'image $\mathbf{I_B}$ :

\begin{equation}
\mathbf{I_B}(1,:) = \frac{\mathbf{\mbox{Proj}_B}(1,2:3)}{\mathbf{\mbox{Proj}_B}(3,2:3)}
\end{equation}
\begin{equation}
\mathbf{I_B}(2,:) = \frac{\mathbf{\mbox{Proj}_B}(2,2:3)}{\mathbf{\mbox{Proj}_B}(3,2:3)}
\end{equation}

Pour terminer, un bruit gaussien en pixel de $\sigma_B$ est aussi appliqué sur la position des marqueurs visuels dans l'image générée $I_B$ :
\begin{equation}
\mathbf{I_B} = \mathbf{I_B} + \mathcal{X}[2,2] \thicksim \mathcal{N}(0, \sigma^2_B).
\end{equation}

Le lecteur peut également consulter l'appendice~\ref{AppendixA} pour y trouver tout le code \emph{Matlab} permettant de réaliser cette simulation. La prochaine sous-section, montre en détail les méthodes utilisées pour valider les calculs servant à localiser le $Robot_B$.

%-----------------------------------
%	MÉTHODES
%-----------------------------------
\subsection{Estimation de $\beta$}
\label{C2S2SS2}
Dans la réalité, la façon d'estimer l'angle $\alpha$ dans l'image $I_B$ est la même que celle décrite à l'équation~\ref{c2s1eqALPHA}. Cependant, la manière d'évaluer l'angle $\beta$ à partir de l'image $I_A$ est légèrement différente que celle présentée à la sous-section~\ref{C2S1SS2}. En effet, l'équation~\ref{c2s1eqBETA} stipule que nous avons besoin du vecteur $^A\vec{P}_{B}$, qui correspond au vecteur en trois dimensions pointant vers la caméra $C_B$. Cependant, les images ne contiennent que les marqueurs visuels, comme ce serait le cas pour une utilisation réelle de notre système. Par conséquent, une estimation de la position projetée de la caméra $C_B$ est faite dans l'image $I_A$. Cette projection, $^AP_C$, correspond à un point sur la droite reliant le marqueur visuel $^AP_R$ (à gauche dans l'image $I_A$) au marqueur visuel $^AP_L$ (à droite dans l'image $I_A$). La position sur cette droite dépend d'un simple calcul proportionnel des distances $d^L_B$ et $d^R_B$, soit :

\begin{equation}
^AP_C \approx \, ^AP_R + \left( \frac{d^R_B}{d^L_B + d^R_B}\right) * (^AP_L - \, ^AP_R)
\label{C2S2SS2approx}
\end{equation}

Cependant, dans une image, les points plus près de la caméra apparaissent plus distants les uns des autres que les points situés aux distances éloignées. Par conséquent, une erreur additionnelle sur l'angle $\beta$ est créée par cette approximation. Pour cette raison, nous avons évalué l'impact de cette erreur sur la localisation relative du $Robot_B$, malgré l'impossibilité dans le cas réel d'utiliser directement le vecteur pointant vers $\vec{C}_B$. Nous avons également comparé cette erreure avec celle obtenue lorsque du bruit est appliqué sur les images synthétisées.

%-----------------------------------
%	RÉSULTATS
%-----------------------------------
\subsection{Résultats et discussion}
\label{C2S2SS3}
Nous avons d'abord généré une simulation dans le cas où les images saisies par les deux robots sont parfaites, c.-à-d. lorsque $\sigma_A = \sigma_B = 0$. Tel que mentionné à la sous-section~\ref{C2S2SS2}, l'angle~$\beta$ est estimé en fonction de la position des marqueurs de l'image $I_A$ prise par le $Robot_A$. Nous avons donc vérifié l'écart entre cette estimation de $\beta$ et la valeur réelle de cet angle mesuré dans l'image $I_A$. La figure~\ref{figC2S2BetaError} présente le ratio de la différence entre l'angle $\beta$ réel et celui estimé à l'aide de l'équation~\ref{C2S2SS2approx}. Nous constatons que dans la majorité des cas, lorsque le $Robot_B$ se trouve à moins de $60\deg$ par rapport au centre de l'image $I_A$, l'erreur sur $\beta$ est nettement inférieure à $0.5\%$ de sa valeur réelle. Ce résultat a été obtenu alors que le $Robot_A$ se trouvait à $10m$ du $Robot_B$ et que nous avons fait varier la position de ce dernier dans l'image $I_A$ d'un angle entre $0\deg$ et $75\deg$ par rapport au centre de cette image.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=5.0cm 0.2cm 5.0cm 0.2cm, clip=true, width=0.97\textwidth]{../Figures/C2S2BetaError.png}
  \end{tabular}
 \end{center}
 \caption[Erreur sur l'estimation de $\beta$]{Erreur sur l'estimation de $\beta$. Nous avons placé le $Robot_B$ à $l=10~m$ du $Robot_A$. Puis nous avons déplacé progressivement le $Robot_B$ à différents endroits dans l'image $I_A$ afin de faire varier $\beta$. Dans la majorité des cas, nous avons une erreur sur $\beta$ de moins de $0.5\%$}
 \label{figC2S2BetaError}
\end{figure}

Lorsque la valeur réelle de l'angle $\beta$ a été utilisée, l'erreur sur la distance $l$, celle sur la position et celles sur l'orientation sont essentiellement nulles ($\simeq 3e^{-11}$) et attribuables à des erreurs d'arrondi. 

Par contre, lorsque l'estimation de $\beta$ est utilisée, ces erreurs de distance~$l$ augmentent en fonction de l'angle entre le $Robot_B$ et l'axe optique de la caméra $C_A$. En effet, plus ce robot est distant du point principal dans l'image $I_A$, plus l'erreur sur $\beta$ est significative. La figure~\ref{figC2S2ErrorNoiseless}~$A$ présente une perspective en deux dimensions de l'impact de l'estimation de l'angle $\beta$ sur le calcul de la distance $l$. La figure~\ref{figC2S2ErrorNoiseless}~$B$ présente elle aussi la même perspective, mais montre l'erreur sur la position. La figure~\ref{figC2S2ErrorNoiseless}~$C$, elle, montre la progression de l'erreur sur l'estimation de l'orientation relative du $Robot_B$. Nous y constatons que pour des caméras parfaites possédant un grand angle de vue et une longueur focale $f_A = f_B = 1000 px$, nous pouvons nous attendre à une erreur sur la distance $l$ plus petite que $1cm$, à une erreur sur la position relative du $Robot_B$ inférieure à $1.5cm$ et à une erreur sur son orientation relative de moins de $0.15\deg$.

%\begin{figure}[ht]
% \begin{center}
%  \begin{tabular}{c}
%    \includegraphics[width=0.97\textwidth]{../Figures/C2S2DistanceErrorNoiseless.png}
%  \end{tabular}
% \end{center}
% \caption[Erreur sur l'estimation de la distance $l$, à $10m$, lorsque $\beta$ est estimé]{Erreur sur l'estimation de la distance $l$, à $10m$, lorsque $\beta$ est estimé. Perspective à deux dimensions présentant l'impact de l'estimation de $\beta$ sur l'erreur en distance. Plus le $Robot_B$ est éloigné du point principal sur l'image $I_A$, plus l'erreur sur l'estimation de la distance augmente. Dans la plupart des cas, l'erreur sur la distance est en dessous de $1cm$.}
% \label{figC2S2DistanceErrorNoiseless}
%\end{figure}

%\begin{figure}[ht]
% \begin{center}
%  \begin{tabular}{c}
%    \includegraphics[width=0.97\textwidth]{../Figures/C2S2PositionErrorNoiseless.png}
%  \end{tabular}
% \end{center}
% \caption[Erreur sur l'estimation de la position relative du $Robot_B$ à $10m$ lorsque $\beta$ est estimé]{Erreur sur l'estimation de la position relative du $Robot_B$ à $10m$ lorsque $\beta$ est estimé. Perspective à deux dimensions présentant l'impact de l'estimation de $\beta$ sur l'erreur en position. Plus le $Robot_B$ est éloigné du point principal sur l'image $I_A$, plus l'erreur sur l'estimation de la position augmente. Dans la plupart des cas, l'erreur sur la distance est en dessous de $1.5cm$.}
% \label{figC2S2PositionErrorNoiseless}
%\end{figure}

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.97\textwidth]{../Figures/C2S2DistanceErrorNoiseless.png}\\
    \includegraphics[width=0.97\textwidth]{../Figures/C2S2PositionErrorNoiseless.png} \\
    \includegraphics[width=0.97\textwidth]{../Figures/C2S2OrientationErrorNoiseless.png}
  \end{tabular}
 \end{center}
 \caption[Erreur sur l'estimation de la pose relative du $Robot_B$ à $10m$ lorsque $\beta$ est estimé]{Erreur sur l'estimation de la pose relative du $Robot_B$ à $10m$ lorsque $\beta$ est estimé par les marqueurs du $Robot_B$. De haut en bas respectivement, la perspective à deux dimensions présentant l'impact de l'estimation de $\beta$ sur l'erreur ($A$)~en distance~$l$, ($B$)~en position et ($C$)~en orientation. Plus le $Robot_B$ est éloigné du point principal sur l'image $I_A$, plus l'erreur sur l'estimation de l'orientation augmente. Dans la plupart des cas, l'erreur sur la distance est en-dessous de $1~cm$, celle sur la position est sous $1.5~cm$ et celle sur l'orientation est de moins de $0.15\deg$.}
 \label{figC2S2ErrorNoiseless}
\end{figure}

Pour estimer l'impact des erreurs de positionnement des points lumineux dans le plan image, nous avons ensuite généré cent mille fois la simulation de la prise d'image. Nous avons instancié les deux robots immobiles à des positions et orientations arbitraires pour des caméras ayant une longueur focale $f_A = f_B = 1000 px$. Nous avons appliqué un bruit gaussien de $\sigma_A = \sigma_B = 0.5px$. %, ce qui résulte en un bruit nettement surestimé par rapport à une situation réelle.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.97\textwidth]{../Figures/C2S2Dist.png}\\
    \includegraphics[width=0.97\textwidth]{../Figures/C2S2Pos.png}\\
    \includegraphics[width=0.97\textwidth]{../Figures/C2S2Ang.png}
  \end{tabular}
 \end{center}
 \caption[Simulation de l'erreur sur la pose relative entre les deux robots]{Simulation de l'erreur sur la pose relative entre les deux robots. Histogramme sur 100 000 échantillons. La simulation a lieu alors que les deux robots sont séparés d'une distance $l$ d'environ 1063 cm. De haut en bas respectivement, l'erreur sur ($A$)~la distance~$l$, ($B$)~la position et ($C$)~l'orientation.}
 \label{figC2S2DistPosAng}
\end{figure}

%\begin{figure}[ht]
% \begin{center}
%  \begin{tabular}{c}
%    \includegraphics[width=0.97\textwidth]{../Figures/C2S2Pos.png}
%  \end{tabular}
% \end{center}
% \caption[Simulation de l'erreur sur la position relative du $Robot_B$]{Simulation de l'erreur sur la position relative du $Robot_B$. Histogramme sur 100 000 échantillons. Les mesures sont en centimètres. La simulation a lieu alors que les deux robots sont séparés d'une distance $l$ d'environ 1063 cm.}
% \label{figC2S2Pos}
%\end{figure}

%\begin{figure}[ht]
% \begin{center}
%  \begin{tabular}{c}
%    \includegraphics[width=0.97\textwidth]{../Figures/C2S2Ang.png}
%  \end{tabular}
% \end{center}
% \caption[Simulation de l'erreur sur l'orientation relative estimée du $Robot_B$]{Simulation de l'erreur sur l'orientation relative estimée du $Robot_B$. Histogramme sur 100 000 échantillons. Les mesures sont en radians. La simulation a lieu alors que les deux robots sont séparés d'une distance $l$ d'environ 1063 cm.}
% \label{figC2S2Ang}
%\end{figure}

La figure~\ref{figC2S2DistPosAng}~$A$ présente l'histogramme de l'erreur sur la distance~$l$ en centimètres résultant de ces simulations. Les résultats indiquent une erreur moyenne sur la distance de $9.1\,cm$ avec un écart-type de $6.88\,cm$, pour une distance~$l$ d'environ $1063~cm$.

De la même manière, la figure~\ref{figC2S2DistPosAng}~$B$ montre un histogramme de l'erreur sur l'estimation de la position relative du $Robot_B$, mesurée en centimètres. Cette erreur, qui a une valeur moyenne de $9.17\,cm$ et un écart-type de $6.83\,cm$, nous confirme que les difficultés de positionnement proviennent principalement de l'erreur sur la distance~$l$ causée par le bruit gaussien dans les images.

Finalement, la figure~\ref{figC2S2DistPosAng}~$C$ présente l'erreur sur l'orientation relative estimée du $Robot_B$ sous forme d'histogramme. Cette orientation estimée possède une erreur moyenne de $0.0028\,rad$ ($0.16\deg$) et un écart-type de $0.002\,rad$ ($0.11\deg$). Similairement, cette erreur est attribuée en majorité au bruit gaussien appliqué sur la position des marqueurs visuels dans les images, tandis qu'une faible minorité de l'erreur est attribuée à l'estimation de l'angle $\beta$.

À la lumière de ces résultats, nous pouvons conclure que l'approximation de l'angle $\beta$, qui provient de l'estimation de la position de la caméra $C_B$ dans l'image $I_A$, n'est pas une source d'erreur importante à cette distance~$l$.

Dans la prochaine section, nous allons décrire les expérimentations qui ont été réalisées avec un prototype de robots utilisant notre système. 