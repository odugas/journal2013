\section{Expérimentations}
\label{Chap2Sec3EXP}

Cette section est consacrée à la description des expérimentations qui ont été réalisées afin de valider le système dans la réalité. En premier lieu, nous allons donner les détails relatifs à la conception et à la construction des dispositifs qui ont été utilisés pour prendre des mesures de poses relatives entre eux. En second lieu, nous allons présenter le système OptiTrack, qui a servi à mesurer la vérité terrain pour estimer les performances réelles de notre approche. En troisième lieu, nous expliquerons la série d'expérimentations qui ont été réalisées et nous analyserons les résultats obtenus.

%-----------------------------------
%        L E S  R O B O T S
%-----------------------------------
\subsection{Conception des prototypes}
\label{C2S3SS1}

Afin de valider les calculs démontrés précédemment, nous avons construit deux prototypes qui ont la capacité de s'observer mutuellement pour estimer leur position relative. Les paragraphes qui suivent expliquent en détail en quoi consistent ces dispositifs.

Chaque dispositif possède une caméra Logitech C905. Une de ces caméras est présentée à la figure~\ref{figC2S3C905}. Celle-ci est compacte et a un faible coût. De plus, une caméra C905 peut afficher des images de grande résolution, soit 1600 pixels de côté par 1200 pixels de haut. De surcroît, la caméra C905 bénéficie d'une très faible distorsion dans ses images. Cependant, lors de la capture d'image, elle utilise le balayage progressif des pixels (\emph{Rolling Shutter}), ce qui provoque des distorsions importantes au niveau des objets qui sont en mouvement dans l'image. Par conséquent, afin d'éliminer toute erreur lors de l'estimation des positions par nos prototypes, choisir ces caméras nous a contraints à prendre des images pour des positions fixes, sans mouvement. Dans le but d'utiliser notre approche en temps réel, il est donc préférable d'utiliser une caméra qui expose tous ses pixels en même temps lors de la capture, technologie appelée le \emph{Global Shutter}.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.4\textwidth]{../Pictures/C905.jpg}
  \end{tabular}
 \end{center}
 \caption{Logitech C905}
 \label{figC2S3C905}
\end{figure}

Des diodes émettant de la lumière (\emph{DEL}) servent de marqueurs lumineux pour les caméras C905. Ces diodes projettent la lumière dans un côte de $40~\deg$ d'angle et requièrent une alimentation de $3.3~V$ à $60~mA$. Ces marqueurs ont été alimentés par une pile $9~V$ pour chaque prototype, selon le schéma présenté à la figure~\ref{figC2S3LED}.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=0.2cm 0.2cm 0.2cm 0.2cm, clip=true, width=0.97\textwidth]{../Figures/C2S3LED.png}
  \end{tabular}
 \end{center}
 \caption[Schéma électronique de l'assemblage des marqueurs lumineux]{Schéma électronique de l'assemblage des marqueurs lumineux sur le $Robot_A$}
 \label{figC2S3LED}
\end{figure}
Les paramètres d'exposition, de gain et de luminosité des caméras C905 ont été ajustés afin de permettre à celles-ci de voir uniquement ces marqueurs lumineux dans un environnement sombre. Ainsi, un simple calcul de centre de masse des pixels blancs permet de retrouver la position de marqueurs lumineux dans une image.

Les DEL et les caméras ont été collées sur des blocs Legos, puis fixées sur une tige d'aluminium par prototype. L'un des prototypes, qui correspond au $Robot_A$, a été immobilisé sur une table à un mètre du sol. Le second prototype, qui correspond au robot mobile $Robot_B$, a été posé sur un trépied d'appareil photo puis placé au centre de la zone observable par le système OptiTrack. Ce trépied possède une tête articulée permettant de faire pivoter le dispositif dans toutes les directions et orientations souhaitées. Des marqueurs OptiTrack ont été ajoutés au dispositif $Robot_B$ de manière asymétrique afin de suivre sa position réelle avec une erreur en dessous du millimètre et son orientation avec une erreur avoisinant les $0.001~rad$. L'image~\ref{figC2S3RobotB} montre l'apparence du prototype $Robot_B$ une fois assemblé, où l'on voit que la caméra a été placée entre les deux marqueurs de sorte que l'œil de la caméra soit colinéaire avec les DEL. Ce positionnement était nécessaire afin de respecter les contraintes décrites à la section~\ref{Chap2Sec1MATH}.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.75\textwidth]{../Pictures/RobotB.png}
  \end{tabular}
 \end{center}
 \caption[Image du prototype $Robot_B$]{Image du prototype $Robot_B$. Le prototype est fixé sur un trépied afin de permettre de le déplacer et puis de le maintenir en place pour faire de l'image par image.}
 \label{figC2S3RobotB}
\end{figure}

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.5\textwidth]{../Pictures/calibration.png}
  \end{tabular}
 \end{center}
 \caption{Une des images ayant servi à calibrer une caméra.}
 \label{figC2S3CamCalib}
\end{figure}

Dans le but d'évaluer les paramètres intrinsèques des caméras C905, un outil de calibration de caméra sous \emph{Matlab} a été employé~\cite{website:calibration-toolbox}. Nous avons pris plusieurs images sous différents angles d'un échiquier avec des cases de trois centimètres de côté (voir l'image~\ref{figC2S3CamCalib}), et cela nous a permis d'estimer la longueur focale $f$, le point principal et les différents paramètres de distorsion de chaque caméra C905.

%-----------------------------------
%	     O P T I T R A C K
%-----------------------------------
\subsection{Le système OptiTrack}
\label{C2S3SS2}

Pour avoir accès à la vérité terrain, nous avons choisi d'utiliser le système de positionnement OptiTrack, de la compagnie NaturalPoint~\cite{website:optitrack}. Ce système permet de localiser des objets dans un volume et de donner la position et l'orientation de ceux-ci avec une très grande précision. Le système comprenait cinq caméras V100:R2 (voir l'image~\ref{figC2S3v100r2}) ainsi que le logiciel \emph{OptiTrack Tracking Tools}. Afin de repérer des objets, de petites sphères réfléchissant l'infrarouge, comme celles présentes dans l'image~\ref{figC2S3RobotB}, sont utilisées. Les caméras V100:R2, qui sont dotées d'un système d'éclairage actif à infrarouges, illuminent ces sphères, ce qui les rend facilement discernables en leur donnant une apparence circulaire dans l'image. En dispersant plusieurs caméras aux extrémités d'un espace à trois dimensions, le système peut ainsi évaluer la position et l'orientation d'objets définis par ces marqueurs à infrarouges.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.2\textwidth]{../Pictures/OptitrackV100R2.jpg}
  \end{tabular}
 \end{center}
 \caption{Caméra V100:R2 d'OptiTrack.}
 \label{figC2S3v100r2}
\end{figure}

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.70\textwidth]{../Pictures/OptitrackHub.png}
  \end{tabular}
 \end{center}
 \caption{L'OptiHub, servant à synchroniser les caméras d'OptiTrack et à gérer le flot de données.}
 \label{figC2S3optihub}
\end{figure}

Étant donné que nous disposions de cinq caméras V100:R2, nous en avons positionné une à chaque coin du plafond de notre espace de vol et nous avons placé la cinquième sur un trépied, qui s'orientait vers le centre de la pièce à partir d'un des côtés de notre espace carré. Ainsi, peu importe la position du $Robot_B$, celui-ci était observé par au moins deux caméras V100:R2, condition nécessaire pour la traque. Nous avons ensuite connecté ces caméras à un ordinateur avec l'aide de deux \emph{OptiHubs}(voir l'image~\ref{figC2S3optihub}), qui s'occupent de la synchronisation des données envoyées par chaque caméra. La figure~\ref{figC2S3Hubs} illustre le diagramme de branchement des hubs avec le poste de travail et les caméras V100:R2.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.70\textwidth]{../Figures/C2S3Hubs.png}
  \end{tabular}
 \end{center}
 \caption[Diagramme de branchement des OptiHubs]{Diagramme de branchement des OptiHubs. Chaque OptiHub est connecté à trois caméras. L'un des OptiHubs joue le rôle de maître et se charge de la synchronisation avec l'OptiHub esclave via un câble de synchronisation. Les données sont transmises à un ordinateur via l'OptiHub maître.}
 \label{figC2S3Hubs}
\end{figure}

La calibration du système est effectuée à l'aide du logiciel \emph{OptiTrack Tracking Tools}. Pour minimiser le bruit lumineux pendant la calibration, les lumières sont éteintes. Ensuite, il suffit de lancer le programme de calibration et de balayer la baguette de calibration, nommée \emph{OptiWand}, dans tout l'espace de vol. Cette baguette, qui est affichée à l'image~\ref{figC2S3optiwand}, a une largeur de $500mm$ et possède trois marqueurs de plus petite taille que les marqueurs courants. Pendant le déplacement de la baguette, le logiciel accumule les données des caméras et les affiche à l'écran (voir l'image~\ref{figC2S3calibration}) afin d'aider l'utilisateur à bien remplir tout le volume couvert par les caméras V100:R2. Par la suite, celles-ci sont utilisées pour effectuer des calculs d'optimisation servant à trouver la pose relative des caméras entre elles. Finalement, la position du sol est définie à l'aide de l'équerre de calibration, qui est présentée à l'image~\ref{figC2S3CalibSquare}, et le système OptiTrack est fin prêt à utiliser.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.70\textwidth]{../Pictures/OptitrackOptiwand.jpg}
  \end{tabular}
 \end{center}
 \caption{L'OptiWand, la baguette de calibration d'OptiTrack.}
 \label{figC2S3optiwand}
\end{figure}

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.70\textwidth]{../Pictures/OptitrackCalibration.png}
  \end{tabular}
 \end{center}
 \caption[Calibration des caméras V100:R2]{Calibration des caméras $V100:R2$. Un affichage montrant en temps réel la zone couverte par l'OptiWand, lors de la calibration, pour chaque caméra.}
 \label{figC2S3calibration}
\end{figure}

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.30\textwidth]{../Pictures/OptitrackCalibrationSquare.jpg}
  \end{tabular}
 \end{center}
 \caption[Équerre de calibration du système OptiTrack]{Équerre de calibration du système OptiTrack. Trois marqueurs y sont apposés.}
 \label{figC2S3CalibSquare}
\end{figure}

Afin d'obtenir la vérité terrain avec ce système lors de nos expérimentations, il est possible de définir des objets à traquer par l'interface graphique du logiciel \emph{OptiTrack Tracking Tools}. Ces objets sont identifiés par plusieurs marqueurs infrarouges, permettant ainsi de déterminer avec précision l'orientation et la position de chaque objet observé par les caméras OptiTrack. Il est ensuite possible d'exporter ces données en temps réel dans \emph{Matlab}. Le lecteur peut également noter que le système d'exploitation pour robots (\emph{ROS}) offre également des librairies pour utiliser les données diffusées en temps réel dans un réseau par le logiciel \emph{OptiTrack Tracking Tools}. Ainsi, nous pouvons connaître la vérité terrain et la comparer avec nos estimations, qui ont été générées par notre système.

Cependant, l'espace couvert par le système OptiTrack ne couvre que trois mètres de côté. Afin de pouvoir faire des expérimentations sur de plus grandes distances, nous avons placé le dispositif de référence $Robot_A$ sur une table à l'extérieur de cette zone, et nous n'avons traqué que la position du prototype $Robot_B$. Ce faisant, le système de coordonnées de la vérité terrain n'était plus le même que celui de notre $Robot_A$. Afin d'aligner ces référentiels tout au long des expérimentations qui ont été réalisées, nous avons fait appel à l'algorithme \emph{Iterative Closest Point}(ICP). Cet algorithme permet d'aligner des modèles tridimensionnels en se basant strictement sur la géométrie des données de ces modèles. L'algorithme, qui a été introduit par Chen et Medioni~\cite{yang1992object} et par Besl et McKay~\cite{besl1992method}, a été dérivé en plusieurs variantes. Pour nos expérimentations, nous avons utilisé une variante qui est disponible dans \emph{Matlab} et qui a été développée par Per Bergström~\cite{website:icpmatlab}.

%-----------------------------------
%   E X P E R I M E N T A T I O N
%-----------------------------------
\subsection{Expérimentations et résultats}
\label{C2S3SS3}

Les expérimentations que nous avons réalisées avaient pour objectif d'analyser la précision de notre solution au problème de la localisation relative à six degrés de liberté, pour un système réel.

Premièrement, il est nécessaire de déterminer la précision de notre vérité terrain, qui nous est fournie par le système OptiTrack. Le tableau~\ref{C2S4SS1tabCalibOptitrack} présente la précision en pixels qui a été obtenue après avoir calibré les cinq caméras V100:R2 utilisées. Comme le lecteur peut le constater, toutes ces caméras ont une erreur sous les 0.08 pixel. De plus, lors de la calibration, nous avons obtenu que l'erreur sur le positionnement de la baguette de calibration OptiWand (illustrée à la figure~\ref{figC2S3optiwand}) était en moyenne de $0.663mm$ avec un écart-type de $0.132mm$.

\begin{table}
	\begin{center}
	\begin{tabular}{|l||c|c|}
	\hline
	Caméra & Moyenne Erreur 2D & Écart-type \\
	 \hline
	 Caméra \#1 & 0.077 pixel & 0.056 pixel \\
	 Caméra \#2 & 0.056 pixel & 0.038 pixel \\
	 Caméra \#3 & 0.076 pixel & 0.048 pixel \\
	 Caméra \#4 & 0.074 pixel & 0.044 pixel \\
	 Caméra \#5 & 0.077 pixel & 0.056 pixel \\
	 \hline
	\end{tabular}
	\caption{\label{C2S4SS1tabCalibOptitrack} Précision des caméras OptiTrack}
	\end{center}
\end{table}

Puisque la position des marqueurs sur l'OptiWand est connue par le système OptiTrack, il fallait s'assurer que la précision sur le positionnement des marqueurs OptiTrack placés sur notre prototype mobile $Robot_B$ était similaire. Pour ce faire, nous avons utilisé l'interface graphique du logiciel \emph{OptiTrack Tracking Tools} pour définir un objet traqué à partir de notre prototype mobile $Robot_B$. Celui-ci était identifié grâce à cinq marqueurs réfléchissants positionnés de façon asymétrique telle qu'illustrée à l'image~\ref{figC2S3RobotB}. À l'aide de ce logiciel, nous avons affecté au prototype $Robot_B$ une orientation initiale ainsi qu'un centre de masse correspondant approximativement à la position de la caméra C905 sur le dispositif. Nous avons obtenu une précision en dessous du millimètre.

Deuxièmement, il a été établi que la précision de notre solution dépend fortement de la résolution des caméras utilisées. À des distances très éloignées, un mouvement des marqueurs lumineux dans les images $I_A$ et $I_B$ de moins d'un pixel a un impact important sur notre approche. En effet, dans la technique d'estimation de la position en deux dimensions~\cite{giguere2012see} que nous employons dans notre solution, l'erreur sur l'estimation de la position est :
\begin{equation}
\sigma_x \propto l \sigma_\varphi
\end{equation}
et
\begin{equation}
\sigma_y \propto l^2 \sigma_\varphi
\end{equation}
où $\sigma_\varphi$ est le bruit en pixel dans les images et où $\sigma_x$ et $\sigma_y$ sont respectivement l'erreur sur la coordonnée de largeur $x$ et de profondeur $y$. De plus, à la section~\ref{C2S2SS3}, nous avons simulé un bruit dans les images prises par des caméras réelles de $\sigma_A = \sigma_B = 0.5px$. Il était donc important de connaître le bruit réel de nos caméras Logitech~C905 afin de nous assurer qu'il ne dépassait pas cette limite. Également, cette évaluation de la précision permet d'utiliser des filtres de Kalman afin d'améliorer l'efficacité de notre solution sur de grandes distances. 

Pour déterminer cette précision, nous avons pris 70 photos du prototype $Robot_B$ avec la caméra $C_A$ de $Robot_A$. Entre chaque image, afin que la distance entre les prototypes demeure sensiblement la même, nous avons bougé de seulement quelques millimètres de côté l'un des prototypes. Les photos ont été saisies dans l'obscurité avec les marqueurs DEL du $Robot_B$, de sorte qu'il soit très facile de calculer les centres de masses de ceux-ci dans les images. Nous avons relevé la distance en pixels entre les marqueurs du $Robot_B$. En cumulant ces valeurs de distance, il était possible d'évaluer la précision en pixel des caméras C905. Nous avons donc développé un script avec le logiciel \emph{Matlab}. Ce script est présenté à l'appendice~\ref{AppendixB}. 

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=4.7cm 0cm 4.7cm 0cm, clip=true, width=0.97\textwidth]{../Figures/C2S4DistanceMarqueurs.png}\\
    \includegraphics[trim=4.7cm 0cm 4.7cm 0cm, clip=true, width=0.97\textwidth]{../Figures/C2S4VariationDistance.png}\\
    \includegraphics[trim=4.7cm 0cm 4.7cm 0cm, clip=true, width=0.97\textwidth]{../Figures/C2S4PrecisionCamera.png}
  \end{tabular}
 \end{center}
 \caption[Évaluation de la précision des caméras C905 de Logitech]{Évaluation de la précision des caméras C905 de Logitech. La figure supérieure ($A$) montre la distance entre les marqueurs lumineux, en pixels, pour chaque image. La figure centrale (B) illustre la variation de cette distance d'une image à l'autre. La figure inférieure (C) est un histogramme qui présente l'erreur en pixels des images. La précision de la caméra~C905 est de $0.19~px$.}
 \label{figC2S4ErrorC905}
\end{figure}

La figure~\ref{figC2S4ErrorC905} présente les résultats de cette expérimentation. En moyenne, la distance entre les marqueurs lumineux des images était de 331.5 pixels. La sous-figure~$A$ montre que l'échantillon n'est pas indépendant et identiquement distribué (\emph{iid}). En effet, les images ont été prises en séquence, et la distance $l$ a varié légèrement lors de l'échantillonnage. Afin d'évaluer l'erreur $\sigma$ en pixels dans l'image, nous avons calculé, pour chaque paire d'images consécutives, la différence des distances inter marqueurs. La sous-figure~$B$ illustre cette variation de distance, d'image en image. Nous remarquons la nature aléatoire de cette variation, ce qui nous permet de considérer que l'échantillon est iid car la partie non iid est négligeable. La sous-figure~$C$ présente l'histogramme de cette variation de la distance entre les marqueurs lumineux. Étant donné que les images n'étaient pas sélectionnées au hasard, ce qui double la variance, nous avons divisé l'écart-type de ces différences par $\sqrt{2}$. L'erreur ainsi obtenue, pour les caméras~C905, est de 0.19 pixel. Cette erreur est près de quatre fois plus élevée que pour les caméras V100:R2 de notre vérité terrain.

Troisièmement, étant donné que nos prototypes sont assemblés sur des tiges d'aluminium, nous devions nous assurer que les marqueurs réfléchissants placés sur le prototype $Robot_B$ étaient suffisamment non colinéaires pour que la vérité terrain retournée par le système OptiTrack nous retourne une orientation précise. Nous souhaitions nous assurer que la forme de notre prototype ou le positionnement asymétrique des marqueurs d'OptiTrack n'entravait pas la précision des mesures des caméras V100:R2. En effet, étant donné que quatre des cinq caméras V100:R2 d'OptiTrack étaient disposées au plafond, et puisque certains réflecteurs n'étaient séparés que d'une dizaine de centimètres, nous devions déterminer si le système OptiTrack risquait de confondre deux marqueurs ou en perdre de vue lorsque le prototype mobile était incliné.

Pour cette expérimentation, nous avons utilisé un téléphone de marque \emph{HTC One V} doté d'une application d'inclinomètre. Tout d'abord, nous avons fixé le téléphone sur la tige d'aluminium du prototype de sorte de connaître l'angle d'inclinaison autour de l'axe $z$, soit celui qui permet de mettre la tige à la verticale au lieu d'à l'horizontale. Nous avons ensuite maintenu l'inclinaison des deux autres axes à zéro et nous avons fait pivoter le $Robot_B$ par itérations de $15\deg$ autour de l'axe $z$. Nous avons enfin analysé les relevés d'orientation du système OptiTrack par les résultats affichés par l'interface graphique du logiciel \emph{OptiTrack Tracking Tools}, mais également à partir des modules OptiTrack utilisables dans \emph{Matlab}, sous l'application de \emph{Simulink}.

Ici, il y a deux faits intéressants que nous avons découverts et que nous souhaitons partager à toute personne souhaitant travailler avec le système OptiTrack. En premier lieu, le lecteur peut noter que les résultats retournés par l'interface du logiciel \emph{OptiTrack Tracking Tools} sont sensiblement différents de ceux retournés par \emph{Matlab}. Cependant, les deux voies de prise de mesures ont la même précision. Nous recommandons donc aux personnes souhaitant utiliser ce système d'utiliser l'une ou l'autre des méthodes d'acquisition de données, tout en gardant ces faits en tête. En dernier lieu, nous nous sommes également aperçus que le format des quaternions utilisés pour relever l'orientation des objets à traquer avec OptiTrack diffère d'une méthode d'acquisition à l'autre. Alors que dans le logiciel \emph{OptiTrack Tracking Tools}, les quaternions sont de la forme $[x y z w]$, ceux retournés par OptiTrack par \emph{Matlab} sont de la forme $[w -x -y -z]$.

\begin{table}
	\begin{center}
	\begin{tabular}{|c||c|c|c|}
	\hline
	Orientation ($\deg$) & OptiTrack ($\deg$) & Notre solution ($\deg$)& Écart ($\deg$) \\
	 \hline
	 15 & 15.2324 & 14.7795 & 0.4529 \\
	 30 & 15.6287 & 15.8420 & 0.2396 \\
	 45 & 14.6917 & 15.2504 & 0.3191 \\
	 60 & 15.0837 & 15.1674 & 0.4028 \\
	 75 & 15.2593 & 15.2517 & 0.3952 \\
	 \hline
	\end{tabular}
	\caption{\label{C2S4SS2RobustesseAngle} Robustesse à l'orientation du système OptiTrack}
	\end{center}
\end{table}

Le tableau~\ref{C2S4SS2RobustesseAngle} illustre la détection des variations de l'orientation du $Robot_B$ après une série de rotation autour de l'axe des Z. La variation de l'orientation est en soit imprécise, puisqu'elle a été réalisée à l'aide d'une application de téléphone portable, qui affichait une précision arrondie au degré près. Néanmoins, dans ce tableau, nous avons comparé la rotation déterminée par le système OptiTrack avec les calculs de localisation relative expliqués à la section~\ref{Chap2Sec1MATH}. Nous avons donc un écart moyen de $0.3619\deg$ entre nos deux systèmes. À la lumière de ces résultats, nous avons donc pu conclure que notre vérité terrain demeurait fiable peu importe l'orientation du $Robot_B$ et qu'aucune confusion entre les marqueurs OptiTrack ne compromettait nos futures expérimentations.

Toutefois, étant donné que les marqueurs lumineux sont indissociables entre eux, notre système de localisation relative n'est pas en mesure d'estimer correctement l'angle $\beta$ ni d'évaluer correctement l'orientation relative du prototype $Robot_B$ si celui-ci est à l'envers. Par conséquent, nous devons limiter les mouvements de ce dernier de sorte que son marqueur lumineux $^A\dot{P}_{R}$ soit toujours plus à gauche dans l'image $I_A$ que son marqueur lumineux $^A\dot{P}_{L}$.

Quatrièmement, nous avons évalué la précision de notre système de localisation relative sur de courtes distances. Cette expérimentation s'est faite en deux étapes. Puisque le référentiel du système de caméras V100:R2 d'OptiTrack n'était pas le même que celui du prototype de référence $Robot_A$, la première étape a été de calculer des estimations de position et d'orientation relatives du $Robot_B$ avec notre méthode tout en relevant simultanément la vérité terrain. Ensuite, afin de convertir les résultats de la vérité terrain dans le référentiel du $Robot_A$, nous avons utilisé l'algorithme \emph{Iterative Closest Point }(ICP). 

Pour le premier ensemble de données, la figure~\ref{FigC2S4CalibXZ} montre une perspective en deux dimensions dans le plan $X-Z$ des positions mesurées et estimées du $Robot_B$ relativement au système de coordonnées du dispositif de référence $Robot_A$. 

\begin{figure}[h]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=1cm 2cm 1cm 2cm, clip=true,  width=0.75\textwidth]{../Figures/C2S4r1xz.png}
  \end{tabular}
 \end{center}
 \caption[Comparaison des positions réelles et estimées dans le plan X-Z avec calibration]{Comparaison des positions réelles et estimées dans le plan X-Z avec calibration. L'algorithme ICP permet d'aligner les deux systèmes de coordonnées, soient celui du $Robot_A$ et le référentiel d'OptiTrack. La distance du $Robot_B$ relativement au $Robot_A$ varie entre 3.5~\emph{m} et 6.5~\emph{m}, une limitation causée par notre système de vérité terrain OptiTrack.}
 \label{FigC2S4CalibXZ}
\end{figure}

La figure~\ref{FigC2S4CalibPos} montre l'erreur sur l'estimation de la position relative du $Robot_B$. Nous avons obtenu, pour cette première expérimentation, une erreur moyenne sur la position légèrement inférieure à $2.5~cm$. Similairement, la figure~\ref{FigC2S4CalibBear} présente l'erreur sur les estimations de l'orientation relative du $Robot_B$. Ici, nous avons évalué que la moyenne de cette erreur est de $0.0370$~\emph{rad}, ce qui correspond à environ $2.12~\deg$. 

\begin{figure}[h]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=1cm 2cm 1cm 2cm, clip=true,  width=0.75\textwidth]{../Figures/C2S4r1p.png}
  \end{tabular}
 \end{center}
 \caption[Erreur sur l'estimation de la position du $Robot_B$ avec calibration]{L'histogramme représente l'erreur sur la position du $Robot_B$ en utilisant l'algorithme ICP pour aligner les deux systèmes de coordonnées, soient celui du $Robot_A$ et le référentiel d'OptiTrack. L'erreur moyenne est de 2.4947~\emph{cm}.}
 \label{FigC2S4CalibPos}
\end{figure}

\begin{figure}[t]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=1cm 2cm 1cm 2cm, clip=true,  width=0.75\textwidth]{../Figures/C2S4r1o.png}
  \end{tabular}
 \end{center}
 \caption[Erreur sur l'estimation de l'orientation du $Robot_B$ avec calibration]{L'histogramme représente l'erreur sur l'orientation du $Robot_B$ en utilisant l'algorithme ICP pour aligner les deux systèmes de coordonnées, soient celui du $Robot_A$ et le référentiel d'OptiTrack. L'erreur moyenne est de 0.0370~\emph{rad}.}
 \label{FigC2S4CalibBear}
\end{figure}

La seconde étape de cette expérimentation a été de prendre un nouvel ensemble d'images en déplaçant le $Robot_B$ pour estimer sa pose relative avec notre système. Puis, en alignant la vérité de terrain avec les estimations grâce aux résultats d'ICP calculés à la première étape, nous avons déterminé la précision de notre solution. 

La figure~\ref{FigC2S4ResultsPos} montre l'erreur sur la position de ce prototype. L'erreur moyenne que nous avons obtenue cette fois est de $1.9885~cm$. De plus, la figure~\ref{FigC2S4ResultsBear} affiche l'erreur sur l'estimation de l'orientation du $Robot_B$, avec une erreur moyenne de $0.0205~rad$, ou $1.175~\deg$. Également, la figure~\ref{FigC2S4ResultsXY} montre le plan X-Z de la dispersion dans l'espace des positions mesurées et estimées du $Robot_B$. Enfin, ces résultats nous montrent que l'erreur sur l'estimation de l'orientation du $Robot_B$ est indépendante de la véritable orientation de celui-ci.

\begin{figure}[t]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=1cm 2cm 1cm 2cm, clip=true,  width=0.75\textwidth]{../Figures/C2S4r2p.png}
  \end{tabular}
 \end{center}
 \caption[Erreur sur l'estimation de la position du $Robot_B$]{Histogramme représentant l'erreur sur l'estimation de la position du $Robot_B$, faite par notre système de localisation relative, pour l'ensemble d'images de test. L'erreur moyenne est de 1.9885 cm.}
 \label{FigC2S4ResultsPos}
\end{figure}

\begin{figure}[t]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=1cm 2cm 1cm 2cm, clip=true, width=0.75\textwidth]{../Figures/C2S4r2o.png}
  \end{tabular}
 \end{center}
 \caption[Erreur sur l'estimation de l'orientation du $Robot_B$]{Histogramme représentant l'erreur sur l'estimation de l'orientation du $Robot_B$, faite par notre système de localisation relative, pour l'ensemble d'images de test. L'erreur moyenne est de 0.0205~\emph{rad}.}
 \label{FigC2S4ResultsBear}
\end{figure}

\begin{figure}[t]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[trim=1cm 2cm 1cm 2cm, clip=true, width=0.75\textwidth]{../Figures/C2S4r2xz.png}
  \end{tabular}
 \end{center}
 \caption[Comparaison des positions réelles et estimées dans le plan X-Z]{Diagramme représentant une comparaison des positions estimées et réelles du $Robot_B$, dans la perspective du plan X-Z du prototype de référence $Robot_A$. La distance entre les deux prototypes varie entre 3.5~\emph{m} et 6.5~\emph{m} suite aux limitations du système OptiTrack.}
 \label{FigC2S4ResultsXY}
\end{figure}

Finalement, nous avons effectué des essais pour analyser le comportement de notre système sur de plus grandes distances. Pour ce faire, le système de OptiTrack ne pouvait pas être utilisé comme réalité de terrain. Celui-ci a donc été remplacé par le système de positionnement Vicon\cite{website:vicon} de l'Université Laval. Puisque le système Vicon dont nous avons bénéficié permettait de couvrir un plus grand espace, nous avons pu vérifier la précision de nos prototypes à des distances relatives de 12 à 15 mètres. 

Comme le fonctionnement du système Vicon est très similaire à celui d'OptiTrack tant en ce qui concerne le matériel que le logiciel, nous référons le lecteur à la sous-section~\ref{C2S3SS2} pour comprendre comment le Vicon a été utilisé.

Tout comme pour le système OptiTrack, le référentiel du système Vicon n'était pas le même que celui du prototype de référence $Robot_A$. Nous avons donc dû employer l'algorithme ICP pour aligner les mesures de notre vérité terrain avec nos estimations de position et d'orientation relatives.

\begin{figure}[h]
 \begin{center}
    \includegraphics[width=1.0\textwidth]{../Figures/C2S4rlxz.png}
 \end{center}
 \caption[Comparaison des positions réelles et estimées dans le plan X-Z sur de longues distances]{Diagramme représentant une comparaison sur de longues distances de la position réelle et estimée dans le plan X-Z en utilisant l'algorithme ICP pour aligner les deux systèmes de coordonnées, soient celui du $Robot_A$ et le référentiel du système Vicon. La distance du $Robot_B$ relativement au $Robot_A$ varie entre 12.1~\emph{m} et 15.0~\emph{m}.}
 \label{FigC2S4rlxz}
\end{figure}

La figure~\ref{FigC2S4rlxz} présente une comparaison des positions réelles et estimées du $Robot_B$ dans le plan X-Z. Ces positions sont affichées relativement au référentiel du $Robot_A$. Puisque le prototype $Robot_B$ était fixé sur un trépied de caméra, sa hauteur relative ne pouvait varier que d'un mètre. C'est pour cette raison que nous avons opté pour la clarté d'un graphique en deux dimensions plutôt que de montrer la distribution tridimensionnelle.

\begin{figure}[t]
 \begin{center}
    \includegraphics[width=1.0\textwidth]{../Figures/C2S4rlp.png}
 \end{center}
 \caption[Erreur sur l'estimation de la position du $Robot_B$ sur de longues distances]{Histogramme représentant l'erreur sur l'estimation de la position du $Robot_B$ sur de longues distances, faite par notre système de localisation relative. L'erreur moyenne est de 5.3687~cm.}
 \label{FigC2S4rlp}
\end{figure}

La figure~\ref{FigC2S4rlp} affiche l'histogramme de l'erreur sur l'estimation de la position du $Robot_B$ relativement au $Robot_A$. L'erreur moyenne est de 5.3687~cm et l'erreur maximale est de 14.2508~cm. Cette augmentation de l'erreur par rapport à nos essais sur de courtes distances peut être expliquée par deux faits. D'abord, la précision de notre solution analytique est dépendante de la distance $l$ qui sépare les prototypes et de la distance $d$ entre les marqueurs de chaque prototype, comme démontré par~\cite{giguere2012see}. En effet, lorsque nous analysons l'erreur sur la distance $l$, nous constatons que celle-ci est en moyenne de  5.1323~cm et au plus de 14.2234~cm. L'augmentation de l'erreur sur la position vient donc principalement de l'erreur sur l'estimation de la distance $l$ entre les deux dispositifs. Ensuite, plus la distance $l$ augmente, plus la précision et la résolution des caméras deviennent déterminantes. Effectivement, si la distance $l$ est grande, les bruits $\sigma_A$ et $\sigma_B$ dans les images $I_A$ et $I_B$ sont plus importants, car un pixel dans ces images couvre une grande distance.

\begin{figure}[t]
 \begin{center}
    \includegraphics[width=1.0\textwidth]{../Figures/C2S4rlo.png}
 \end{center}
 \caption[Erreur sur l'estimation de l'orientation du $Robot_B$ sur de longues distances]{Histogramme représentant l'erreur sur l'estimation de l'orientation du $Robot_B$ sur de longues distances, faite par notre système de localisation relative. L'erreur moyenne est de 0.0238~\emph{rad}.}
 \label{FigC2S4rlo}
\end{figure}

La figure~\ref{FigC2S4rlo} montre l'erreur sur l'évaluation de l'orientation relative du $Robot_B$ par rapport au $Robot_A$. Nous avons obtenu que la moyenne de cette erreur est de $0.0238~\emph{rad}$, ou $1.3635~\deg$, et que l'erreur maximale est de $0.0885~\emph{rad}$, ou $5.0731~\deg$. Cette erreur est donc demeurée similaire à celle obtenue lors des expérimentations à courte portée. Cela s'explique notamment par la précision des caméras utilisées.

Dans la prochaine section, nous allons généraliser de notre solution au cas non colinéaire, c'est-à-dire lorsque la caméra $C_i$ de chaque prototype $Robot_i$ n'est pas fixée directement sur l'axe entre les deux marqueurs de celui-ci.