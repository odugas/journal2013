\section{Généralisation au problème non colinéaire}
\label{Chap2Sec5COLINEAR}

Cette section est consacrée à la description de l'extension de notre solution analytique au cas non colinéaire, c'est-à-dire lorsque les marqueurs $\vec{R}_i$ et $\vec{L}_i$ de chaque robot ne sont pas sur le même axe que la caméra $\vec{C}_i$ de leur robot respectif. En premier lieu, nous précisons les implications mathématiques de cette généralisation. En second lieu, nous développons les corrections mathématiques qui peuvent être appliquées à notre solution analytique pour minimiser l'erreur de nos estimations. En troisième lieu, nous présentons les résultats des simulations qui ont été réalisées pour valider nos calculs. Nous concluons en ouvrant sur les possibilités de cette généralisation.

%-----------------------------------
%           E F F E T S
%-----------------------------------
\subsection{Effets de la non-colinéarité sur la solution proposée}
\label{C2S5SS1}

Le fait que les caméras ne soient pas colinéaires avec les marqueurs lumineux de leur robot respectif ajoute une complexité aux calculs de localisation relative. Afin de bien comprendre cette complexité, nous allons présenter les effets du décalage des caméras sur la solution que nous avons proposée à la section~\ref{Chap2Sec1MATH}. L'analyse de ces effets est d'abord faite en ne supposant que le décalage d'une caméra à la fois. Ensuite, nous vérifions ce qui doit être considéré lorsque les deux caméras ne sont pas colinéaires.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.50\textwidth]{../Figures/C2S5OffsetA.png}
  \end{tabular}
 \end{center}
 \caption[Visualisation de la non-colinéarité selon le plan $Y-Z$ dans le cas de la caméra $\vec{C}_A$]{Visualisation de la non-colinéarité selon le plan $Y-Z$ dans le cas de la caméra $\vec{C}_A$. Cette caméra observe une translation $\vec{S}_A$ par rapport au centre du référentiel $F_A$. De plus, cette translation possède un angle de $\Theta_A$ par rapport au vecteur $^A\vec{P}_B$.}
 \label{figC2S5OffsetA}
\end{figure}

Tout d'abord, considérons le cas où seulement la caméra $\vec{C}_A$ n'est pas sur l'axe défini par $\vec{R}_A$ et $\vec{L}_A$. La figure~\ref{figC2S5OffsetA} montre graphiquement ce décalage, qui peut être complété par un rappel à la figure~\ref{C2S1FigAxeA}. Soit un plan de décalage défini par le vecteur $^A\vec{P}_B$ et par la normale du plan illustré en jaune à la figure~\ref{C2S1FigAxeA}. Nous définissons, sur ce plan de décalage, une translation $\vec{S}_A$, qui rend la caméra $\vec{C}_A$ non colinéaire. Alors, l'angle de $\vec{S}_A$ par rapport à $^A\vec{P}_B$ peut être décrit par la variable $\Theta_A$.

D'une part, il a été vu à la sous-section~\ref{C2S1SS2} que l'angle $\alpha$ correspond à l'angle entre les deux marqueurs du $Robot_A$ vus de la caméra $\vec{C}_B$. D'autre part, nous avons également montré que  l'angle $\beta$ correspond à l'orientation de la caméra $\vec{C}_B$ par rapport à l'axe optique de la caméra $\vec{C}_A$. Si la caméra $\vec{C}_A$ est décalée, cela entraîne la modification de la mesure de $\beta$. Cependant, celle de l'angle $\alpha$ demeure inchangée. Cela a pour conséquence que l'angle $\alpha$ engendre une erreur puisqu'il ne considère pas le décalage de $\vec{C}_A$. 

Dans cette situation, étant donné que l'angle $\beta$ prend en compte la position non colinéaire de la caméra $\vec{C}_A$, la direction de $^A\vec{P}_B$ est bien déterminée. Néanmoins, l'équation~\ref{c2s1eqDISTANCE} nous montre que la distance estimée $l$ qui sépare les deux caméras est mal calculée, puisqu'elle dépend de l'angle $\alpha$ et que celui-ci est erroné. Afin de corriger cette erreur, il faut donc modifier la longueur $l$. De plus, la translation appliquée à la caméra $\vec{C}_A$ modifie légèrement l'estimation de l'orientation relative de la caméra $\vec{C}_B$. Ainsi, il est nécessaire de déterminer la rotation permettant de corriger cette erreur.

Ensuite, considérons le cas où la caméra $\vec{C}_B$ n'est pas sur l'axe défini par  $\vec{R}_B$ et $\vec{L}_B$ tout en supposant que la caméra $\vec{C}_A$ demeure colinéaire. De façon similaire, le plan de décalage est cette fois défini par le vecteur $^B\vec{P}_A$ et par la normale du plan illustré en jaune à la figure~\ref{C2S1FigAxeB}. Nous définissons, sur ce plan de décalage, une translation $\vec{S}_B$, qui rend la caméra $\vec{C}_B$ non colinéaire. Alors, l'angle de $\vec{S}_B$ par rapport à $^B\vec{P}_A$ peut être décrit par la variable $\Theta_B$. Étant donné que ces définitions sont les mêmes que pour le cas précédent, nous omettons d'ajouter une autre figure et nous référons le lecteur à la figure~\ref{figC2S5OffsetA}.

Il a été vu à la sous-section~\ref{C2S1SS2} que l'image $I_B$ générée par la caméra $\vec{C}_B$ est utilisée pour calculer l'angle $\alpha$. Puisque la caméra $\vec{C}_B$ est décalée, la valeur de l'angle $alpha$ est modifiée. Cependant, la caméra $\vec{C}_A$ utilise les marqueurs $\vec{R}_B$ et $\vec{L}_B$ pour évaluer la direction de $^A\vec{P}_B$ dans le but de déterminer l'angle $\beta$. Par conséquent, l'angle $\beta$ est erroné dans cette situation. Néanmoins, puisque $\alpha$ est altéré par le décalage de la caméra $\vec{C}_B$, la distance estimée $l$ est reste calculée correctement. Afin de corriger cette erreur, il suffit donc de calculer la rotation à appliquer pour modifier l'estimation de la position relative de la caméra $\vec{C}_B$. De surcroît, il faut appliquer cette même rotation à l'estimation de son orientation relative.

\begin{center}
  \begin{tabular}{| l || c | c |}
    \hline
    Perturbation sur & Caméra $C_A$ & Caméra $C_B$ \\ \hline
    Affecte l'angle & $\alpha$ & $\beta$ \\ \hline
    Altère l'estimation de & distance $l$ & position \\ \hline
    Correction sur $^A_B\mathbf{T}$ par & translation & rotation \\ \hline
    Correction sur $^A_B\mathbf{R}$ par & petite rotation & petite rotation \\ \hline
  \end{tabular}
  \label{TableC2S5SS1Perturbation}
\end{center}

Le tableau~\ref{TableC2S5SS1Perturbation} est une synthèse de la présente sous-section. Nous y avons affiché les effets respectifs de décalages appliqués à chaque caméra. Nous rappelons que la matrice $^A_B\mathbf{T}$ est définie par la conversion du vecteur de translation $\vec{T}$ à l'équation~\ref{C2S1SS3T} en une matrice de translation. Nous indiquons également que la matrice $^A_B\mathbf{R}$ est en fait la combinaison des matrices $^A_B\mathbf{R1}$ et $^A_B\mathbf{R2}$, qui proviennent respectivement des équations~\ref{C2S1SS3R1} et~\ref{C2S1SS3R2}. 

Nous notons ici une conclusion importante : le décalage d'une caméra entraîne des effets indépendants de la colinéarité de l'autre caméra. Ainsi, nous allons utiliser ce fait à notre avantage à la prochaine sous-section afin de déterminer les corrections mathématiques à appliquer à notre solution analytique pour résoudre le problème non colinéaire.

%-----------------------------------
%       C O R R E C T I O N S
%-----------------------------------
\subsection{Corrections mathématiques à notre solution}
\label{C2S5SS2}

Tout d'abord, lorsque la caméra $\vec{C}_A$ n'est pas colinéaire, la valeur de l'angle $\alpha$ est invalide. Cette erreur a un impact sur l'estimation de la distance $l$ entre les caméras. Dans cette situation, l'erreur peut être corrigée par une translation le long du vecteur $\vec{T}$ (qui a été déterminé à l'équation~\ref{C2S1SS3T}) et par une faible rotation pour corriger l'orientation relative estimée du $Robot_B$. Supposons que le décalage de la caméra correspond à une translation, $\vec{S}_A$, qui est connue dans le référentiel $F_A$. Dans le but de déterminer la correction à appliquer, nous devons retrouver l'angle $\Theta_A$ entre le décalage $\vec{S}_A$ de la caméra $\vec{C}_A$ et le vecteur $^A\vec{P}_B$. $^A\vec{P}_B$ n'est pas connu par notre système, mais il est néanmoins possible de l'estimer par~$\vec{T}$. 

\begin{equation}
	\Theta_A = \mbox{acos} (\frac{\vec{T} \cdot \vec{S}_A}{|\vec{T}||\vec{S}_A|}).
	\label{C2S5SS2ThetaA}
\end{equation}

Identifions par $\vec{T}_\epsilon$ la translation corrigeant l'estimation initiale de la translation entre les deux robots. En utilisant l'angle estimé $\Theta_A$, $\vec{T}_\epsilon$ est calculée en retrouvant l'erreur $\epsilon_l$ sur la distance $l$ entre le $Robot_A$ et le $Robot_B$ :

\begin{equation}
	\epsilon_l = |\vec{S}_A|\cos(\Theta_A)
\end{equation}
\begin{equation}
	\vec{T}_\epsilon = \epsilon_l\frac{\vec{T}}{|\vec{T}|}
	\label{C2S5SS2Teps}
\end{equation}

Par la suite, $\vec{T}_\epsilon$ peut être trivialement convertie en une matrice de translation $\mathbf{T}_\epsilon$. De manière similaire, nous retrouvons la petite rotation qui corrige la non-colinéarité de la caméra $\vec{C}_A$. Cette rotation, définie par $\mathbf{R}^A_\epsilon$, est déterminée en utilisant encore une fois l'angle $\Theta_A$, qui a été calculé à l'équation~\ref{C2S5SS2ThetaA}.

\begin{equation}
	\mathbf{R}^A_\epsilon = \mbox{Rodrigues}\left(\vec{L_AR_A}, \arcsin\left(\sin(\Theta_A)\frac{|\vec{S}_A|}{|\vec{T}|+\epsilon_l}\right)\right)
	\label{C2S5SS2RepsA}
\end{equation}

Avec ces deux matrices, soient $\vec{T}_\epsilon$ et $\mathbf{R}^A_\epsilon$, nous pouvons modifier l'équation~\ref{C2S1SS3Align}, qui permet d'aligner les référentiels $F_B$ et $F_A$, afin de retrouver la pose relative du $Robot_B$ tout en considérant la non-colinéarité de la caméra $\vec{C}_A$.

\begin{equation}
\label{C2S5SS2CA}
\,^AF_B = \mathbf{T}_\epsilon \, ^A_B\mathbf{T} \mathbf{R}^A_\epsilon \, ^A_B\mathbf{R_2} \, ^A_B\mathbf{R_1} F_B'
\end{equation}

Ensuite, il nous faut considérer la non-colinéarité de la caméra $\vec{C}_B$. Comme expliqué à la sous-section~\ref{C2S5SS1}, lorsque cette caméra n'est pas colinéaire, la valeur de l'angle $\beta$ est invalide. Cela a un impact sur l'estimation de la position du $Robot_B$, mais la distance $l$ demeure bien estimée. Dans cette situation, l'erreur peut être corrigée par une rotation autour de l'axe $\vec{L_AR_A}$ ainsi que par une faible rotation pour corriger l'orientation relative estimée du $Robot_B$.  Supposons que le décalage de la caméra correspond à une translation, $\vec{S}_B$, qui est connue dans le référentiel $F_B$. Dans le but de déterminer la correction à appliquer, nous devons retrouver l'ange $\Theta_B$ entre le décalage $\vec{S}_B$ de la caméra $\vec{C}_B$ et le vecteur $^B\vec{P}_A$. Tout comme pour $^A\vec{P}_B$, $^B\vec{P}_A$ n'est pas connu par notre système. Cependant, nous pouvons l'estimer par~$-\vec{T}$, que nous allons exprimer dans le référentiel $F_B$. Ainsi, nous obtenons :

\begin{equation}
\Theta_B = \arccos \left( \frac{^A_B\mathbf{R_1}^{-1}\,^A_B\mathbf{R_2}^{-1}(-\vec{T})}{|\vec{T}|} \cdot \frac{\vec{S}_B}{|\vec{S}_B|} \right)
\label{C2S5SS2ThetaB}
\end{equation}

Nous rappelons ici au lecteur que $^A_B\mathbf{R_1}$ et $^A_B\mathbf{R_2}$ ont respectivement été définis par les équations \ref{C2S1SS3R1} et~\ref{C2S1SS3R2} à la sous-section~\ref{Chap2Sec1SS3}. 

Suite au calcul de $\Theta_B$ à l'équation~\ref{C2S5SS2ThetaB}, nous pouvons déterminer la matrice de rotation $\mathbf{R}^B_\epsilon$ qui permet de corriger la translation estimée $\vec{T}$ ainsi que l'orientation relative estimée $^A_B\mathbf{R_2}^A_B\mathbf{R_1}$. Cette matrice $\mathbf{R}^B_\epsilon$ est calculée en retrouvant l'erreur $\epsilon_\theta$ sur l'orientation résultant de la non-colinéarité de la caméra $\vec{C}_B$.

\begin{equation}
	\epsilon_\theta = \arcsin(\sin(\Theta_B)\frac{|\vec{S}_B|}{|\vec{T}|+\epsilon_l})
\end{equation}
\begin{equation}
	\mathbf{R}^B_\epsilon = \mbox{Rodrigues}(\vec{L_AR_A}, \epsilon_\theta)
	\label{C2S5SS2RepsB}
\end{equation}

Puisque nous avons montré à la sous-section précédente que le décalage d'une caméra entraîne des effets indépendants de la colinéarité de l'autre caméra, nous pouvons ajouter à l'équation~\ref{C2S5SS2CA} les corrections $\mathbf{R}^B_\epsilon$. Cela nous amène à l'équation~\ref{C2S5SS2NonColinear}, qui permet d'estimer la pose relative du $Robot_B$ par rapport au référentiel du $Robot_A$ tout en considérant la non-colinéarité des caméras avec les marqueurs de leur robot respectifs.
\begin{equation}
\label{C2S5SS2NonColinear}
\,^AF_B = \mathbf{R}^B_\epsilon \mathbf{T}_\epsilon \,^A_B\mathbf{T} \mathbf{R}^B_\epsilon \mathbf{R}^A_\epsilon \,^A_B\mathbf{R_2} \,^A_B\mathbf{R_1} F_B'
\end{equation}


%-----------------------------------
%       S I M U L A T I O N S
%-----------------------------------
\subsection{Simulations et Résultats}
\label{C2S5SS3}

Dans le but de vérifier l'impact de la non-colinéarité sur les erreurs de localisation, nous avons implémenté une simulation dans \emph{Matlab}. Dans cette sous-section, nous allons détailler ce qui a été simulé et nous présentons une analyse de nos résultats. Nous invitons le lecteur à consulter l'Appendice~\ref{AppendixC} pour plus de détails sur l'implémentation de la simulation qui a été développée.

Avant de débuter les différentes vérifications, nous souhaitons rappeler au lecteur qu'à la sous-section~\ref{C2S2SS3}, nous avons montré que même lorsque les deux caméras sont colinéaires, la valeur de l'angle $\beta$ est estimée à partir de la position des marqueurs lumineux $R_B$ et $L_B$. Cette estimation de $\beta$ engendre une erreur sur la position de $0.5717cm$ et sur l'orientation de $0.0534\deg$ lorsque la distance $l$ qui sépare les robots est d'environ $11m$ et lorsque les marqueurs de chaque robot sont séparés par une distance $d$ de $80cm$. Ces valeurs servent donc d'étalon pour évaluer nos corrections à la colinéarité.

Premièrement, nous avons exécuté la simulation en la configurant de sorte que seule la caméra $\vec{C}_A$ soit non colinéaire. La translation $\vec{S}_A$ que nous lui avons appliquée avait une norme variant entre 5\% de la distance $d$, soit $4cm$, et 35\% de $d$ ou $28cm$, lorsque $d=80cm$. $\vec{S}_A$ est orientée dans une direction allant entre $0 rad$ et $\pi rad$ dans le plan défini par les vecteurs $^A\vec{P}_{B}$ et $\vec{n}_A$ (nous référons le lecteur aux équations \ref{C2S1SS2APB} et \ref{C2S1SS3nA}, respectivement). Nous avons observé le comportement des corrections mathématiques décrites à l'équation~\ref{C2S5SS2CA}. Comme le montre la figure~\ref{figC2S5OffsetAOnly}, les erreurs maximales lorsque le décalage est de 35\% de $d$ sont de $0.5893cm$ sur la position et de $0.1486\deg$ sur l'orientation. 

\begin{figure}[ht]
 \begin{center}
    \includegraphics[width=1.0\textwidth]{../Figures/C2S5AoffsetOnlyNoNoise.png}
 \end{center}
 \caption[Erreurs sur la localisation relative sans colinéarité pour la caméra $\vec{C}_A$]{Graphique représentant les erreurs sur la position et sur l'orientation relatives du $Robot_B$ lorsque la caméra $\vec{C}_A$ n'est pas colinéaire. Chaque courbe représente les valeurs d'erreurs en fonction de la norme du vecteur de translation $\vec{S}_A$ appliqué pour décaler la caméra $\vec{C}_A$. L'erreur maximale sur la position avec $28cm$ de décalage est de $0.5893cm$ et celle sur l'orientation est de $0.1486\deg$.}
 \label{figC2S5OffsetAOnly}
\end{figure}

Deuxièmement, nous avons refait la simulation en faisant en sorte que seule la caméra $\vec{C}_B$ ne soit pas colinéaire. Les mêmes paramètres ont été observés pour déterminer la nature de la translation $\vec{S}_B$ comme pour $\vec{S}_A$ à l'étape précédente. Nous avons fait varier la norme et l'orientation de cette translation afin d'observer le comportement de l'erreur après avoir appliqué les corrections définies par l'équation~\ref{C2S5SS2RepsB}. Comme le montre la figure~\ref{figC2S5OffsetBOnly}, les erreurs maximales lorsque le décalage est de 35\% de $d$ sont de $2.8838cm$ sur la position et de $0.1517\deg$ sur l'orientation. Nous constatons que bien que l'erreur est corrigée, elle reste cependant cinq fois plus élevée pour la position que lorsque seule la caméra $\vec{C}_A$ avait été décalée. Cette situation découle du fait que l'angle $\beta$ joue un rôle important sur l'erreur du positionnement. En effet, plus $\beta$ est grand, plus une faible incertitude sur cet angle a un grand impact sur la position estimée. Ce phénomène a été montré par~\cite{giguere2012see}.

\begin{figure}[ht]
 \begin{center}
    \includegraphics[width=1.0\textwidth]{../Figures/C2S5BoffsetOnlyNoNoise.png}
 \end{center}
 \caption[Erreurs sur la localisation relative sans colinéarité pour la caméra $\vec{C}_B$]{Graphique représentant les erreurs sur la position et sur l'orientation relatives du $Robot_B$ lorsque la caméra $\vec{C}_B$ n'est pas colinéaire. Chaque courbe représente les valeurs d'erreurs en fonction de la norme du vecteur de translation $\vec{S}_B$ appliqué pour décaler la caméra $\vec{C}_B$. L'erreur maximale sur la position avec $28cm$ de décalage est de $2.8838cm$ et celle sur l'orientation est de $0.1517\deg$.}
 \label{figC2S5OffsetBOnly}
\end{figure}

Troisièmement, afin de montrer que les erreurs pouvent être corrigées en les considérant comme indépendantes grâce à l'équation~\ref{C2S5SS2NonColinear}, nous avons simulé le cas où les deux caméras, $\vec{C}_A$ et $\vec{C}_B$, étaient non colinéaires. La figure~\ref{figC2S5ABOffset} présente les résultats de cette simulation. Lorsque les vecteurs de translation $\vec{S}_A$ et $\vec{S}_B$ ont une norme de $28cm$, ce qui correspond à 35\% de la distance $d$ entre les marqueurs d'un robot, nous obtenons une erreur maximale sur la position de $2.9543cm$ et une erreur maximale sur l'orientation de $0.3095\deg$. Il nous est au passage possible de constater que cela correspond approximativement à la somme des erreurs mesurées lorsque les caméras étaient non colinéaires une à la fois. Par conséquent, cette simulation confirme la validité de l'équation~\ref{C2S5SS2NonColinear}.

\begin{figure}[ht]
 \begin{center}
    \includegraphics[width=1.0\textwidth]{../Figures/C2S5ABoffsetsNoNoise.png}
 \end{center}
 \caption[Erreurs sur la localisation relative sans colinéarité]{Graphique représentant les erreurs sur la position et sur l'orientation relatives du $Robot_B$ lorsque les caméras $\vec{C}_A$ et $\vec{C}_B$ ne sont pas colinéaire. Chaque courbe représente les valeurs d'erreurs en fonction de la norme des vecteurs de translation $\vec{S}_A$ et $\vec{S}_B$ appliquée pour décaler respectivement les caméras $\vec{C}_A$ et $\vec{C}_B$. L'erreur maximale sur la position avec $28cm$ de décalage est de $2.9543cm$ et celle sur l'orientation est de $0.3095\deg$.}
 \label{figC2S5ABOffset}
\end{figure}

Quatrièmement, il nous fallait étudier l'efficacité de la correction de l'estimation de la position lorsque les deux caméras ne sont pas colinéaires. L'un des motifs justifiant cette nouvelle simulation est que nous avions observé, tant dans le cas à deux dimensions que dans notre situation actuelle, que plus l'angle $\beta$ était grand, plus l'erreur sur le positionnement pouvait croître rapidement lorsque $\beta$ était mal estimé. De surcroît, nous avons constaté lors des simulations précédentes que la non-colinéarité de la caméra $\vec{C}_B$ rendait plus difficile la correction de l'estimation de la position. Afin de mieux analyser cette situation, nous avons placé le $Robot_B$ à $10m$ du $Robot_A$ et nous avons fait varier la position du premier en le faisant se déplacer dans l'image prise par la caméra $\vec{C}_A$. Nous l'avons progressivement éloigné du centre de l'image $I_A$ dans le premier quadrant, tant sur l'axe des abscisses que sur l'axe des ordonnées, d'un angle variant de $0\deg$ à $75\deg$. Tout en déplaçant le $Robot_B$ de la sorte, nous nous sommes assurés de le conserver à la distance de $10m$ du $Robot_A$. Au terme de cette simulation, nous nous attendions à observer que plus l'angle $\beta$ augmentait, plus l'erreur après la correction serait grande. De plus, étant donné que le $Robot_B$ était orienté pour avoir ses marqueurs lumineux à l'horizontale dans l'image $I_A$, l'angle $\beta$ tel que décrit à l'équation~\ref{c2s1eqBETA} allait être plus grand lorsque le $Robot_B$ serait déplacé plus loin l'axe $X$ que sur l'axe $Y$. Par conséquent, nous imaginions que l'erreur sur la position allait augmenter plus rapidement pour un déplacement en $X$ dans l'image $I_A$. C'est effectivement ce que nous avons observé, comme le montre la figure~\ref{figC2S5PositionNoNoise}. Effectivement, lorsque le $Robot_B$ est centré dans l'image $I_A$, l'erreur sur la position est près de $1cm$. Inversement, lorsque l'angle $\beta$ est à sa valeur la plus grande, l'erreur sur son estimation engendre une erreur sur le positionnement de $23.2cm$. Cependant, une telle valeur ne survient que lorsque $\beta$ est d'environ $75\deg$, soit lorsque les caméras s'observent à des angles extrêmes. Étant donné que nous utilisons pour notre système réel que des caméras qui possèdent un demi-angle de vue de 50 degrés ou moins, nous pouvons nous attendre à une erreur sur le positionnement en deçà de $3cm$ dans la plupart des cas d'après les résultats de notre simulation.

\begin{figure}[ht]
 \begin{center}
    \includegraphics[width=1.0\textwidth]{../Figures/C2S5ErrorPositionNoNoise.png}
 \end{center}
 \caption[Variation de l'erreur sur la position avec le déplacement dans l'image $I_A$ du $Robot_B$]{Variation de l'erreur sur la position avec le déplacement dans l'image $I_A$ du $Robot_B$. Le déplacement en $X$ fait augmenter rapidement la valeur de l'angle $\beta$. Cette augmentation a pour effet de hausser l'erreur sur la position, tel que démontré précédemment par~\cite{giguere2012see}.}
 \label{figC2S5PositionNoNoise}
\end{figure}

 Finalement, nous voulions analyser le comportement de la correction de l'erreur lorsque les caméras sont bruitées. Nous avons appliqué un bruit de $\sigma_A=0.0003rad$  dans l'image $I_A$ pour la mesure de l'angle $\beta$ et de $\sigma_B= \sqrt{2} * 0.0003rad$ dans l'image $I_B$ pour la mesure de l'angle $\alpha$. Le bruit sur l'image $I_B$ est plus grand car il faut deux positions dans cette image pour calculer l'angle $\alpha$. Tout comme pour la précédente simulation, nous avons déplacé le $Robot_B$ dans l'espace en le conservant à $10m$ du $Robot_A$. La figure~\ref{figC2S5PositionWithNoise} affiche les résultats obtenus et les compare aux erreurs obtenues lorsqu'il n'y a pas de bruit sur les caméras. Les résultats indiquent que dans la plupart des positions, l'erreur sur le positionnement causé par la non-colinéarité vaut pour moins de $40\%$ du bruit qui est induit par les caméras. 
 
\begin{figure}[ht]
 \begin{center}
    \includegraphics[width=1.0\textwidth]{../Figures/C2S5ErrorPositionWithNoise.png}
 \end{center}
 \caption[Variation de l'erreur sur la position avec le déplacement dans l'image $I_A$ du $Robot_B$ et avec bruit]{Variation de l'erreur sur la position avec le déplacement dans l'image $I_A$ du $Robot_B$ et avec bruit. En bleu, nous illustrons l'erreur après la correction pour la non-colinéarité. En rouge, nous affichons l'impact du bruit des les caméras sur l'erreur des estimations. Le bruit est de $\sigma_A = \sigma_B = 0.3$. déplacement en $X$ fait augmenter rapidement la valeur de l'angle $\beta$. Cette augmentation a pour effet de hausser l'erreur sur la position, comme démontré précédemment par~\cite{giguere2012see}.}
 \label{figC2S5PositionWithNoise}
\end{figure}
 
 Par conséquent, nous pouvons en conclure que l'erreur causée par la non-colinéarité des caméras n'affecte pas la performance du système. Cependant, cette erreur doit être considérée comme un biais sur l'estimation de la pose relative du $Robot_B$. Ainsi, il faut en tenir compte si nous utilisons un tel système avec une technique de filtrage probabiliste, comme un filtre étendu de Kalman ou un filtre de Kalman non parfumé, pour améliorer la précision du positionnement sur de longues distances.


%-----------------------------------
%       C O N C L U S I O N
%-----------------------------------
\subsection{Conclusion}
\label{C2S5SS4}

À la prochaine et dernière section de ce chapitre, nous allons faire un retour sur ce qui a été vu dans chacune des sections de celui-ci. Nous mettrons aussi en perspective les possibilités de cette nouvelle solution analytique au problème de la localisation coopérative en trois dimensions. 

