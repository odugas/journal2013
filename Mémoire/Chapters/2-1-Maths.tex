\section{Solution analytique au problème}
\label{Chap2Sec1MATH}

Le problème de la localisation coopérative en trois dimensions n'utilisant que des mesures d'angle peut être décrit comme suit. Il est à noter que cette solution analytique en trois dimensions est une amélioration de la solution pour le cas en deux dimensions développée par Philippe Giguère, Ioannis Rekleitis et Maxime Latulippe~\cite{giguere2012see}. Nous faisons référence à cette solution à plusieurs reprises dans les prochaines sous-sections.

Étant donné que cette section décrit une solution analytique impliquant différents types de variables, il s'avère nécessaire de définir une nomenclature. D'abord, toute position à deux dimensions dans une image est exprimée par un \emph{point} au-dessus de variable, p. ex.~$\dot{X}$. Ensuite, les vecteurs en trois dimensions sont représentés par une \emph{flèche}, comme~$\vec{X}$. Puis, une matrice est plutôt définie par une variable affichée en \emph{gras} comme~$\mathbf{X}$. Également, tout vecteur unitaire servant d'axe de rotation possède comme distinction un \emph{chapeau} au-dessus de sa variable, p. ex.~$\hat{X}$. Aussi, les scalaires représentés par l'italique. Il en va finalement de même pour les référentiels et autres éléments, mentionnés par souci de clarté, mais n'appartenant pas à la solution analytique.

%-----------------------------------
%	DÉFINITION DU PROBLÈME
%-----------------------------------
\subsection{Définition du problème}
\label{Chap2Sec1SS1}
Soit deux robots, $Robot_A$ et $Robot_B$, libres de se déplacer dans tous les six degrés de liberté, c'est-à-dire que ces robots peuvent se déplacer en position [x,y,z] et en orientation [roulis, tangage, lacet]. Chaque robot $i$, où $i \in \left\{ A,B  \right\}$, est équipé d'une caméra et de deux marqueurs visuels, $\vec{R}_i$ et $\vec{L}_i$. Les marqueurs visuels sont placés à une distance $d$ l'un de l'autre. La caméra, elle, est fixée solidement sur chaque robot et placée de sorte que son centre de projection $\vec{C}_i$ passe exactement par la mi-distance séparant les marqueurs du robot $i$. Chaque robot possède son propre référentiel local $F_i$ tel que l'origine de chaque référentiel ait comme origine son centre de projection $\vec{C}_i$ respectif, avec l'axe des $z$ déterminé comme l'axe optique, l'axe des $y$ pointant à la verticale et l'axe des $x$, à l'horizontale. Dans le but de respecter la règle de la main droite, il est posé que les $z$ positifs vont vers le fond de l'image, que les $y$ positifs vont vers le haut et que les $x$ positifs, vers la gauche. Ainsi, dans un référentiel local, les deux marqueurs sont situés à~:

$$
\vec{R}_i = [-\frac{d}{2}\mbox{  }0\mbox{  }0], \qquad \vec{L}_i = [\frac{d}{2}\mbox{  }0\mbox{ }0].
$$

Il est important de noter que la coordonnée $x$ du marqueur droit $\vec{R}_i$ est négative, comme stipulé par l'orientation du référentiel $F_i$. La figure~\ref{figC2S1BothRobots} est référée au lecteur afin de lui offrir une représentation graphique du problème. Finalement, il est supposé dans ce problème qu'en toutes circonstances, la caméra d'un robot peut voir l'autre robot et les deux marqueurs de ce dernier. Si cette dernière condition n'est pas remplie, la localisation est quand même possible si le robot partiellement observable a trois marqueurs non colinéaires. Cependant, la précision de l'estimation sera moindre.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.5\textwidth]{../Figures/C2BothRobots.png}
  \end{tabular}
 \end{center}
 \caption[Problème de la localisation relative en trois dimensions]{Problème de la localisation relative en trois dimensions. Deux robots $A$ et $B$ opérant sous 6 degrés de liberté. Dans cette représentation graphique du problème, seuls les marqueurs $\vec{R}_A$ et $\vec{L}_A$ du robot $A$ sont affichés. Les lignes rouges et vertes représentent les rayons lumineux entre ces marqueurs et le centre de projection $\vec{C}_B$, située à l'origine du référentiel $F_B$.}
 \label{figC2S1BothRobots}
\end{figure}

La localisation relative entre le $Robot_A$ et le $Robot_B$ est calculée en utilisant deux images prises simultanément par chacune des caméras. Si les images ne sont pas prises en même temps, aucune des équations mathématiques qui suivent n’est applicable. Définissons l'image saisie par le $Robot_A$ par $I_A$, et faisons de même pour l'image du $Robot_B$ en définissant $I_B$. Alors, chaque robot $i$, et donc par extensions ses marqueurs $L_i$ et $R_i$, sont présents dans l'image capturée par l'autre robot. En d'autres termes, $L_B$ et $R_B$ se trouvent dans l'image $I_A$ enregistrée par la caméra du $Robot_A$ et vice versa. Cette situation est graphiquement exposée par la figure~\ref{figC2S1BothRobots}.

L'orientation et la position relatives entre les référentiels $F_A$ et $F_B$ peuvent être définies par deux transformations, soit une matrice de translation $^A_B\mathbf{T}$ et une matrice de rotation $^A_B\mathbf{R}$. Les informations disponibles permettant de déterminer ces transformations sont~:

\begin{itemize}
\item la position des marqueurs $\vec{R}_i=[-\frac{d}{2}\mbox{  }0\mbox{  }0]$ et $\vec{L}_i= [\frac{d}{2}\mbox{  }0\mbox{ }0]$ à l'intérieur du référentiel de leur robot respectif $F_i$;
\item les paramètres internes de calibration de chaque caméra;
\item la position du sous-pixel $^A\dot{P}_{R}$ et $^A\dot{P}_{L}$ dans l'image $I_A$ représentant les marqueurs $\vec{R}_B$ et $\vec{L}_B$, respectivement;
\item la position du sous-pixel $^B\dot{P}_{R}$ et $^B\dot{P}_{L}$ dans l'image $I_B$ représentant les marqueurs $\vec{R}_A$ et $\vec{L}_A$, respectivement.
\end{itemize}

En se basant uniquement sur les informations susmentionnées, il est possible d'inférer la position approximative de la caméra du robot opposé dans une image, sachant que cette dernière a été fixée, lors de la construction du robot, entre les deux marqueurs du robot. Ces positions sont~:

\begin{itemize}
\item la position du sous-pixel dans l'image $I_A$ de $\vec{C}_B$
\begin{equation}
^A\dot{P}_{B} \approx 0.5(^A\dot{P}_{R}+^A\dot{P}_{L});
\end{equation}
\item et la position du sous-pixel dans l'image $I_B$ de $\vec{C}_A$
\begin{equation}
^B\dot{P}_{A}\approx 0.5( ^B\dot{P}_{R}+ ^B\dot{P}_{L}).
\end{equation}
\end{itemize}

Ces dernières approximations sont valides lorsque les robots sont suffisamment éloignés l'un de l'autre, c.-à-d. $l \gg d$. La nomenclature $^i\dot{P}_X$ exprimée ici indique la position du point $X$ dans l'image $Image_i$, $X$ étant ici la caméra $\vec{C}_A$, la caméra $\vec{C}_B$ ou encore l'un des marqueurs $\vec{R}_i$ ou $\vec{L}_i$, dans le référentiel de $i$.

Le problème de localisation relative étant maintenant posé, la prochaine sous-section expliquera la stratégie employée pour retrouver la matrice de translation $\mathbf{T}$ et celle de rotation $\mathbf{R}$ permettant de résoudre ce système.

%-----------------------------------
%	RÉDUCTION 2D
%-----------------------------------
\subsection{Réduction à un problème en deux dimensions}
\label{C2S1SS2}
Considérons maintenant un plan défini dans l'espace à trois dimensions par les trois points colinéaires $\vec{R}_A$, $\vec{L}_A$ et $\vec{C}_A$ sur le $Robot_A$, ainsi que par le centre de la caméra fixée au $Robot_B$, $\vec{C}_B$. Les points $\vec{R}_A$, $\vec{L}_A$ et $\vec{C}_A$ sont véritablement colinéaires en raison de notre conception initiale du système, telle qu'établie à la section~\ref{Chap2Sec1SS1}. Notez que l'origine dans les deux référentiels, $F_A$ et $F_B$, font également partie de ce plan, puisque l'origine d'un référentiel $F_i$ est définie par le centre de projection $\vec{C}_i$ d'une des caméras. Ce plan est la clé de notre technique. Effectivement, cette dernière consiste à réduire une partie du problème à trois dimensions à un problème en deux dimensions. Une fois cette étape accomplie, nous bénéficions de la solution établie par~\cite{giguere2012see}. Cette solution, qui est expliquée dans les prochains paragraphes pour des fins de complétude du présent ouvrage, permet notamment d'estimer la distance entre les deux caméras $l=|\vec{C}_A\vec{C}_B|$.

La figure~\ref{C2S1FigAxeA} illustre la stratégie de réduction en deux dimensions du point de vue de la caméra $\vec{C}_A$, et montre où se trouve le plan en deux dimensions, ici dessiné en jaune. Également, la figure~\ref{C2S1FigAxeB} montre ce même plan, mais du point de vue de la caméra $\vec{C}_B$.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.5\textwidth]{Figures/C2S1AxisA.png}
  \end{tabular}
 \end{center}
 \caption[Point de vue de la caméra $\vec{C}_A$ dans le problème de localisation relative en trois dimensions]{Point de vue de la caméra $\vec{C}_A$ dans le problème de localisation relative en trois dimensions. L'axe $z$, qui pointe vers le fond de l'image, représente l'axe optique de la caméra $\vec{C}_A$. Le lecteur peut remarquer que l'axe $x$ pointe vers la gauche afin de respecter la règle de la main droite des systèmes de coordonnées cartésiennes. La surface rose correspond au plan image $I_A$ et le point $^A\dot{P}_B$ est situé sur ce plan. Le plan en jaune est le même plan que dans la figure~\ref{C2S1FigAxeB}. Le vecteur $^A\vec{P}_B$, qui interprète en trois dimensions le point $^A\dot{P}_B$, se situe sur ce plan et pointe vers la caméra $\vec{C}_B$.}
 \label{C2S1FigAxeA}
\end{figure}

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.5\textwidth]{Figures/C2S1AxisB.png}
  \end{tabular}
 \end{center}
 \caption[Point de vue de la caméra $\vec{C}_B$ dans le problème de localisation relative en trois dimensions]{Point de vue de la caméra $\vec{C}_B$ dans le problème de localisation relative en trois dimensions. L'axe $z$, qui pointe vers le fond de l'image, représente l'axe optique de la caméra $\vec{C}_B$. Cette caméra voit les deux marqueurs du robot opposé, $\vec{L}_A$ et $\vec{R}_A$, de même que l'autre caméra $\vec{C}_A$. Les points $^B\dot{P}_{R}$, $^B\dot{P}_{A}$ et $^B\dot{P}_{L}$ sont situés dans le plan image, affiché ici en rose. Leur interprétation en trois dimensions forme un plan, ici en jaune, permettant la réduction au problème à deux dimensions.}
 \label{C2S1FigAxeB}
\end{figure}

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.50\textwidth]{../Figures/C2S1phi.png}
  \end{tabular}
 \end{center}
 \caption[Les trois angles $\varphi$ mesurés par une caméra]{Les trois angles $\varphi$ mesurés par une caméra. Les angles $\varphi_1$ et $\varphi_3$ correspondent aux positions angulaires des marqueurs visuels dans l'image. $\varphi_2$, lui, représente l'angle dans l'image de la caméra opposée.}
 \label{figC2S1phi}
\end{figure}

Tel que cela a été expliqué par~\cite{giguere2012see}, nous supposons deux robots qui s'orientent de sorte que la caméra $\vec{C}_A$ peut voir les marqueurs $\vec{L}_B$ et $\vec{R}_B$. Puisque la caméra $\vec{C}_B$ se trouve entre ces marqueurs, nous posons par extension que la caméra $\vec{C}_A$ peut également voir $\vec{C}_B$. Supposons également que la caméra $\vec{C}_B$ observe le robot $Robot_A$ de la même façon. À partir de cette configuration, on peut extraire des images $I_A$ et $I_B$ six mesures d'angle, soit $\varphi_1^A$, $\varphi_2^A$, $\varphi_3^A$, $\varphi_1^B$, $\varphi_2^B$ et $\varphi_3^B$. La figure~\ref{figC2S1phi} montre que ces angles $\varphi$ correspondent aux angles des trois points d'intérêts du robot opposé en partant de l'axe optique de la caméra qui capture l'image. Par la suite, deux angles sont déduits pour permettre la localisation du $Robot_B$ relativement au $Robot_A$ en deux dimensions~:

\begin{itemize}
\item l'angle $\alpha = |\varphi_1^B - \varphi_3^B|$, qui correspond à l'angle entre les deux marqueurs du $Robot_A$ vus de la caméra $\vec{C}_B$;
\item l'angle $\beta = \varphi_2^A$, qui correspond à l'angle sur la position de la caméra $\vec{C}_B$ par rapport à l'axe optique de la caméra $\vec{C}_A$.
\end{itemize}

Ces deux angles induisent les contraintes énumérées ci-dessous en lien avec le positionnement du $Robot_B$ par rapport au $Robot_A$. 

\begin{itemize}
\item selon les théorèmes de l'angle inscrit et de l'angle au centre, on peut déterminer que la caméra $\vec{C}_B$ doit être située sur un cercle de rayon $r = \frac{d}{2\sin\alpha}$, cercle passant par $\vec{L}_A$ et par $\vec{R}_A$.
\item la caméra $\vec{C}_B$ doit nécessairement se trouver le long de la ligne d'angle $\beta$ dans le référentiel de la caméra $\vec{C}_A$
\end{itemize}

Il est à noter que les auteurs ont eu recours aux théorèmes de l'angle inscrit et de l'angle au centre. Le théorème de l'angle au centre utilisé ci-dessus affirme que dans un cercle, un angle au centre mesure le double d'un angle inscrit interceptant le même arc. Le théorème de l'angle inscrit, lui, affirme que deux angles inscrits sur un même cercle et interceptant le même arc de cercle ont la même mesure.

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.50\textwidth]{../Figures/C2S1Cercle.png}
  \end{tabular}
 \end{center}
 \caption[Localisation coopérative en deux dimensions]{Localisation coopérative en deux dimensions. Le $Robot_A$ est défini par sa caméra $\vec{C}_A$ et ses deux marqueurs $\vec{R}_A$ et $\vec{L}_A$, séparé par une distance $d$. Le $Robot_B$ est quant à lui défini par la caméra $\vec{C}_B$ et les marqueurs $\vec{R}_B$ et $\vec{L}_B$. Les angles $\alpha$ et $\beta$, mesurés par les caméras $\vec{C}_B$ et $\vec{C}_A$ respectivement, sont utilisés dans notre solution afin de retrouver la distance $l$ séparant les robots.}
 \label{figC2S1Cercle}
\end{figure}

La figure~\ref{figC2S1Cercle} illustre le problème de localisation en deux dimensions tel que présenté par~\cite{giguere2012see}. Toujours selon les auteurs et d'après les inductions établies ci-haut, la distance $l = |\vec{C}_A\vec{C}_B|$ entre les deux robots peut ainsi être calculée. Nous allons donc évaluer cette distance. Cependant, il nous faut d'abord déterminer les angles $\alpha$ et $\beta$ dans la formulation en trois dimensions du problème auquel nous nous sommes attaqué. Par conséquent, l'angle $\alpha = \widehat{\vec{L}_A\vec{C}_B\vec{R}_A}$ de la figure~\ref{figC2S1Cercle} est calculé en créant les vecteurs 3D partant de l'origine du plan image $I_B$ ainsi~:

\begin{equation}
^B\vec{P}_{R} = [ ^B\dot{P}_{Ru} \quad ^B\dot{P}_{Rv} \quad f_B]
\label{c2s1eqBPR}
\end{equation}
et
\begin{equation}
^B\vec{P}_{L} = [ ^B\dot{P}_{Lu} \quad ^B\dot{P}_{Lv} \quad f_B]
\label{c2s1eqBPL}
\end{equation}

Ici, $f_B$ est la longueur focale de la caméra $\vec{C}_B$. Notez également que les indices $u$ et $v$ représentent les coordonnées en pixels dans l'image $I_B$. Il suffit ensuite de calculer l'angle $\alpha$ en trouvant l'angle de rotation entre ces deux vecteurs 3D, qui est calculé comme suit~:

\begin{equation}
\alpha = \arccos(\frac {^B\vec{P}_{L} \cdot ^B\vec{P}_{R}}{|^B\vec{P}_{L}||^B\vec{P}_{R}|} )
\label{c2s1eqALPHA}
\end{equation}

La prochaine étape est de calculer l'angle $\beta$. Pour ce faire, nous avons besoin d'exprimer deux vecteurs à trois dimensions qui passent par le même plan en deux dimensions que les vecteurs qui ont permis de calculer $\alpha$. Les deux vecteurs dont nous avons besoin sont l'axe optique $z$ de la caméra $C_A$ projeté sur ce plan et le vecteur $^A\vec{P}_B$ allant vers la caméra $\vec{C}_B$. D'abord, $^A\vec{P}_B$ se détermine de la même manière que pour les équations~\ref{c2s1eqBPR} et~\ref{c2s1eqBPL}~:

\begin{equation}
^A\vec{P}_{B} = [ ^A\dot{P}_{Bu} \quad ^A\dot{P}_{Bv} \quad f_A]
\label{C2S1SS2APB}
\end{equation}

où $u$ et $v$ représentent les coordonnées en pixels dans l'image $I_A$ et où $f_A$ correspond à la longueur focale de la caméra $\vec{C}_A$. Pour ce qui est de l'axe optique de la caméra $C_A$ projeté sur le plan, ce vecteur est plus complexe à déterminer. Alors, une stratégie est utilisée pour éviter ce calcul. D'abord, nous savons que l'angle $\alpha$ est évalué à l'aide des vecteurs 3D $^B\vec{P}_{R}$ et $^B\vec{P}_{L}$ situés sur le plan 2D. De plus, la caméra $\vec{C}_A$ est contenue dans ce plan puisqu'elle est colinéaire aux points $L_A$ et $R_A$. Aussi, rappelons que le vecteur $\vec{R}_A$ est un vecteur à trois dimensions passant par $\vec{C}_A$ et allant au point $R_A$. Par définition, nous avons que $\vec{R}_A$ est sur le plan 2D et que ce vecteur est perpendiculaire à l'axe optique dans $I_A$ projeté sur le plan. Il s'en suit naturellement la définition de l'équation permettant de calculer l'angle $\beta$, telle qu'illustrée par la figure~\ref{figC2S1Beta}~:

\begin{equation}
\beta = \frac{\pi}{2} - \arccos(\frac{^A\vec{P}_{B} \cdot \vec{R}_A } {|^A\vec{P}_{B}| |\vec{R}_A|} ),
\label{c2s1eqBETA}
\end{equation}

\begin{figure}[ht]
 \begin{center}
  \begin{tabular}{c}
    \includegraphics[width=0.50\textwidth]{../Figures/C2S1Beta.png}
  \end{tabular}
 \end{center}
 \caption[Définition de l'angle $\beta$]{Définition de l'angle $\beta$ tel qu'observé dans le plan 2D.}
 \label{figC2S1Beta}
\end{figure}

En bref, l'angle $\alpha$ est déterminé à partir de l'image $I_B$ tandis que l'angle $\beta$, lui, l'est à partir de l'image $I_A$. Ces deux angles, qui ont été évalués grâce aux équations~\ref{c2s1eqALPHA} et~\ref{c2s1eqBETA}, sont tous les deux mesurés dans un même plan, réduisant ainsi le problème à trois dimensions en un problème à deux dimensions. Ce faisant, il est possible d'utiliser $\alpha$, $\beta$ et la distance $d$ entre les marqueurs $\vec{L}_A$ et $\vec{R}_A$ pour calculer la distance $l$ entre les deux caméras en utilisant la solution géométrique présentée par~\cite{giguere2012see}~:

\begin{equation}
l =\frac{d}{2 \sin\alpha} \left ({ \cos\alpha \cos\beta + \sqrt{1-\cos^2\alpha \sin^2\beta}} \right )
\label{c2s1eqDISTANCE}
\end{equation}

Maintenant que nous avons exploité le problème à deux dimensions pour déterminer la distance $l$ entre les deux caméras, il est désormais nécessaire d'étendre le problème pour travailler en trois dimensions, afin de déterminer la véritable pose relative de la caméra $\vec{C}_B$.

%-----------------------------------
%	EXTENSION 3D
%-----------------------------------
\subsection{Extension à un problème en trois dimensions}
\label{Chap2Sec1SS3}
Le problème de la localisation relative en trois dimensions consiste à trouver une matrice de rotation $^A_B\mathbf{R}$ et une matrice de translation $^A_B\mathbf{T}$ entre les deux référentiels $F_A$ et $F_B$. Ce faisant, il devient possible d'exprimer toute position ou orientation connue dans le référentiel $F_B$ de sorte qu'elle le soit également dans le référentiel $F_A$. Un fait important à noter est qu'il existe de nombreuses façons de déterminer la matrice de rotation $^A_B\mathbf{R}$ en vertu du principe des angles d'Euler. En effet, ce principe veut qu'une rotation dans l'espace à trois dimensions puisse se définir par une séquence de rotations dites élémentaires, c'est-à-dire par des rotations autour des axes du référentiel choisi. De la même manière, au lieu de rechercher une seule matrice de rotation, il est possible de calculer une série de rotations plus faciles à visualiser en fonction du problème actuel. Dans ce mémoire, nous avons choisi de ne démontrer que la séquence de rotation que nous avons jugée la plus simple à comprendre et la plus aisée à calculer.

Avant de débuter, les trois contraintes suivantes sont utilisées pour estimer $^A_B\mathbf{R}$ et $^A_B\mathbf{T}$~:
\begin{itemize}
\item $C_1$ : La caméra $\vec{C}_B$ est localisée, dans le référentiel $F_A$, le long du vecteur $^A\vec{P}_{B}$ à une distance $l$ déterminée à la sous-section~\ref{C2S1SS2} (voir l'équation~\ref{c2s1eqDISTANCE});
\item $C_2$ : Le plan défini dans le référentiel $F_B$ qui contient les vecteurs $^B\vec{P}_{R}$ et $^B\vec{P}_{L}$ est coplanaire avec le plan défini dans le référentiel $F_A$ par les vecteurs $^A\vec{P}_{B}$ et $\vec{R}_A$;
\item $C_3$ : Le vecteur $^A\vec{P}_{B}$ défini dans $F_A$ et le vecteur $^B\vec{P}_{A}$ défini dans $F_B$ sont spatialement colinéaires et vont dans des directions opposées.
\end{itemize}

De façon similaire, il est important de remarquer le plan passe par la coordonnée de la caméra locale $(0,0,0)$ de chacun des deux plans $F_A$ et $F_B$.

Tout d'abord, nous commençons par créer un nouveau référentiel intermédiaire que nous appelons $F'_B$. Ce référentiel coïncide au départ avec la position et l'orientation du référentiel $F_A$ et contient tous les points qui ont été trouvés dans $F_B$, soit $^B\vec{P}_{A}$, $^B\vec{P}_{L}$ et $^B\vec{P}_{R}$. L'objectif ici est donc de trouver les transformations nécessaires afin que $F'_B$ soit à la même position et la même orientation que $F_B$, mais cette fois dans le référentiel de $F_A$.

Ensuite, partons du principe qu'un plan peut être défini de façon unique par deux vecteurs linéairement indépendants. Nous avons le premier plan, qui est défini dans le référentiel $F_A$, tel qu'illustré à la figure~\ref{C2S1FigAxeA}, par la paire de vecteurs $\vec{R}_A$ et $^A\vec{P}_{B}$. Nous avons aussi le second plan, qui est défini dans le référentiel $F'_B$ et $F_B$, par les vecteurs $^B\vec{P}_{R}$ et $^B\vec{P}_{L}$ tel qu'illustré à la figure~\ref{C2S1FigAxeB}. Afin de satisfaire la contrainte $C_2$, les deux plans dans $F_B$ et $F'_B$ doivent être alignées grâce à une première matrice de rotation, $^A_B\mathbf{R_1}$. Comme ces deux plans passent par l'origine de leur référentiel respectif, aucune matrice de translation n'est nécessaire à cette étape pour les aligner. 

Pour calculer cette matrice $^A_B\mathbf{R_1}$, nous allons utiliser une représentation de rotation, nommée rotation axe-angle, qui émane du théorème de rotation d'Euler. Ce théorème implique que n'importe quelle rotation peut être exprimée par un axe immuable et par une rotation autour de celui-ci. Donc, pour aligner le plan de $F'_B$ avec le plan de $F_A$, nous allons plutôt aligner les vecteurs normaux de ces plans. Ces vecteurs sont le vecteur normal $\vec{n}_B$ du plan dans le référentiel $F'_B$ et le vecteur normal $\vec{n}_A$ du plan dans le référentiel $F_A$. La normale $\vec{n}_A$ est déterminée par le produit croisé suivant~:

\begin{equation}
\vec{n}_A = \vec{R}_A \times \,^A\vec{P}_{B}
\label{C2S1SS3nA}
\end{equation}

Ce produit va toujours pointer vers le haut, c.-à-d. $\vec{n}_A$ a toujours une valeur $y$ positive, puisque $\vec{R}_A$ pointe toujours vers la droite (les $x$ négatifs) et parce qu'une longueur focale est toujours positive par définition ($f_A > 0$). Dans le référentiel $F'_B$, nous pouvons utiliser la paire de vecteurs linéairement indépendants $^B\vec{P}_{R}$ et $^B\vec{P}_{L}$ pour définir le second plan, qui est coplanaire au premier tel qu'illustré par la figure~\ref{C2S1FigAxeB}. La normale $\vec{n}_B$ peut être définie par l'équation suivante~:

\begin{equation}
\vec{n}_B = \, ^B\vec{P}_{L} \times \, ^B\vec{P}_{R}
\end{equation}

Par la suite, on peut voir que les deux normales, $\vec{n}_A$ et $\vec{n}_B$, forment elles-mêmes un plan, sauf si les vecteurs ne sont pas linéairement indépendants. Ce cas ne nous intéresse pas, car il nous donnerait ultimement le cas d'une rotation nulle, qui est un sous-cas trivial du problème actuel et qui, de toute façon, est inclus dans le cas général expliqué ici. Le lecteur peut également constater que l'ordre des vecteurs dans le produit croisé ci-dessus est important afin d'avoir encore une fois une normale possédant une valeur $y$ positive. Le cas échéant, aligner les normales en utilisant la notation axe-angle résulterait en deux normales qui, bien que linéairement dépendantes, iraient dans des directions opposées, faussant ainsi les calculs subséquents. Nous pouvons donc trouver la normale $\hat{e}_1$ du plan décrit par les normales $\vec{n}_A$ et $\vec{n}_B$~:

\begin{equation}
\hat{e}_1 = (\frac{\vec{n}_B \times \vec{n}_A}{|\vec{n}_B||\vec{n}_A|})
\end{equation}

Il est à noter que le choix de rendre unitaire le vecteur $\hat{e}_1$ permet de simplifier les calculs subséquents. Par la même occasion, cela permet l'utilisation de la représentation compacte des rotations axe-angle, $\hat{e}_1$ étant donc un vecteur identifiant l'axe de rotation. Il est ensuite possible de donner à $\hat{e}_1$ une norme différente de l'unité afin d'indiquer par cette nouvelle norme un angle de rotation. Pour déterminer l'angle de rotation autour de l'axe $\hat{e}_1$ permettant d'aligner $\vec{n}_B$ sur $\vec{n}_A$, il suffit d'utiliser la loi des cosinus afin d'isoler l'angle~:

\begin{equation}
\Theta_1 = \arccos (\frac{\vec{n}_B \cdot \vec{n}_A}{|\vec{n}_B||\vec{n}_A|})
\end{equation}

Maintenant que nous avons déterminé la rotation permettant d'aligner les plans des deux repères $F_A$ et $F'_B$, nous devons convertir cette représentation axe-angle pour trouver la matrice de rotation $^A_B\mathbf{R_1}$. Pour ce faire, il faut simplement utiliser la formule de rotation de \emph{Rodrigues}~:

\begin{equation}
^A_B\mathbf{R_1} = I + [\hat{e}_1]_x + (1 - \cos\Theta_1 )(\Theta_1\Theta_1^T - I)
\end{equation}

où $I$ est la matrice identité de taille $3x3$. Par souci de simplicité, nous allons référer à cette formule ainsi~:

\begin{equation}
^A_B\mathbf{R_1} = \mbox{Rodrigues}(\hat{e}_1,\Theta_1).
\label{C2S1SS3R1}
\end{equation}

Une fois arrivés à cette étape, nous avons deux référentiels, soit $F_A$ et $^A_B\mathbf{R_1}F'_B$. Le plan dans chaque référentiel est coplanaire avec le plan dans l'autre référentiel, mais les vecteurs $^A_B\mathbf{R_1} ^B\vec{P}_{A}$ et $^A\vec{P}_{B}$ ne sont toujours pas alignés. Afin de satisfaire la contrainte $C_3$, l'étape suivante est donc de trouver la matrice de rotation $^A_B\mathbf{R_2}$ permettant d'aligner $^A_B\mathbf{R_1} ^B\vec{P}_{A}$ avec $-^A\vec{P}_{B}$. Ici, le signe négatif exprime le fait que dans le monde réel, $^A\vec{P}_{B}$ et $^B\vec{P}_{A}$ ont la même orientation, mais une direction opposée. L'axe de rotation $\hat{e}_2$ de cette seconde rotation est la normale du plan dans le référentiel $F_A$~:

\begin{equation}
\hat{e}_2 = \vec{n}_A.
\end{equation}

Ensuite, il faut déterminer l'angle de rotation nécessaire pour aligner ces deux vecteurs, ce qui est calculé ainsi~:

\begin{equation}
\Theta_2 = \arccos(\frac{-^A\vec{P}_{B} \cdot ^A_B\mathbf{R_1}^B\vec{P}_{A}}{|^A\vec{P}_{B}| |^B\vec{P}_{A}|})
\end{equation}

qui peut être réécrit, selon les propriétés des cosinus, de la façon suivante~:

\begin{equation}
\Theta_2 = \pi - \arccos(\frac{^A\vec{P}_{B} \cdot ^A_B\mathbf{R_1}^B\vec{P}_{A}}{|^A\vec{P}_{B}| |^B\vec{P}_{A}|}).
 \end{equation}
 
 Nous avons donc la seconde rotation, sous sa forme axe-angle, qui doit être appliquée à $^A_B\mathbf{R_1}F'_B$ afin que l'orientation des vecteurs $^A_B\mathbf{R_1} ^B\vec{P}_{A}$ et $-^A\vec{P}_{B}$ soit la même. Afin d'appliquer la rotation, nous devons la convertir en matrice de rotation grâce à la formule de Rodrigues~:
 
 \begin{equation}
 ^A_B\mathbf{R_2} = \mbox{Rodrigues}(\hat{e}_2,\Theta_2).
 \label{C2S1SS3R2}
 \end{equation}
 
Finalement, la dernière étape est d'appliquer une translation à $^A_B\mathbf{R_2R_1}F_B'$ afin de placer le référentiel $F_B'$ à sa position relative par rapport au référentiel $F_A$ et dans le but de satisfaire la contrainte $C_1$. Cela est aisé à réaliser, car nous avons $^A\vec{P}_{B}$ qui correspond à la direction en trois dimensions où devrait se trouver le référentiel $F_B$. De surcroît, nous avons également calculé, à l'équation \ref{c2s1eqDISTANCE}, la distance $l$ entre les deux caméras, $\vec{C}_A$ et $\vec{C}_B$, qui sont les origines des référentiels $F_A$ et $F_B$ par définition. La matrice de translation est donc simplement déterminée par~: 

\begin{equation}
 \vec{T} = l \frac{^A\vec{P}_{B}}{|^A\vec{P}_{B}|}.
 \label{C2S1SS3T}
\end{equation}
 
Enfin, il suffit de convertir $\vec{T}$ en une matrice de translation $^A_B\mathbf{T}$ et de convertir les rotations $^A_B\mathbf{R_2}$ et $^A_B\mathbf{R_1}$ pour en faire des matrices $4x4$, ce qui est une manipulation triviale. Ainsi, nous avons retrouvé la combinaison de transformation permettant d'exprimer la position et l'orientation du $Robot_B$ dans le repère du robot $Robot_A$. Par conséquent, nous obtenons une estimation de la pose relative de chacun d'eux. En d'autres termes, nous avons su déterminer :
 
\begin{equation}
 \,^AF_B = ^A_B\mathbf{T}^A_B\mathbf{R}F_B' = ^A_B\mathbf{T}^A_B\mathbf{R_2}^A_B\mathbf{R_1}F_B',
 \label{C2S1SS3Align}
\end{equation}
 
où $^AF_B$ est la position et l'orientation de $F_B$ exprimées dans le système de coordonnées de $F_A$, et où $F'_B$ a été transformé à partir de $F_B$ pour coïncider avec $^AF_B$. 
 
 %-----------------------------------
 %	Conclusion
 %-----------------------------------
 \subsection{Conclusion}
 \label{Chap2Sec1SS4}
 
 Dans cette section, nous avons démontré mathématiquement comment il est possible, en n'utilisant que des mesures d'angle ainsi qu'une mesure de distance entre deux marqueurs visuels, de déterminer la pose relative de deux robots s'observant mutuellement. Après avoir défini le problème à la sous-section~\ref{Chap2Sec1SS1}, nous avons montré à la sous-section~\ref{C2S1SS2} comment il était possible de réduire la difficulté du problème à trois dimensions en le ramenant à un à deux dimensions. Ensuite, en utilisant cette stratégie, il a été possible d'estimer la distance entre le $Robot_A$ et le $Robot_B$. Finalement, nous avons fait usage d'une combinaison simple de deux rotations et d'une translation afin de parvenir à exprimer le référentiel $F_B$ du $Robot_B$ dans le référentiel $F_A$ du $Robot_A$, ce qui nous donne une approche claire pour calculer la pose relative d'un robot par rapport à un autre. Dans la prochaine section, nous allons décrire comment une simulation par ordinateur a été réalisée afin de vérifier la validité de ces calculs, et aussi pour permettre d'évaluer l'impact des incertitudes sur la localisation relative.
